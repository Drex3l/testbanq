<div id="modal_img" class="modal display-none">
  <div class="modal-content">
    <div class="modal-header">
      <h2 id="modal_img_title"></h2>
    </div>
      <div id="modal_crop_body" class="modal-body clearfix">
      <div id="modal_crop_space" class="clearfix crop-space"></div>
      <span class="left img-rotate" title="90"><i class="fa fa-rotate-left"></i></span>
      <span class="right img-rotate" title="-90"><i class="fa fa-rotate-right"></i></span>
    </div>
    <div class="modal-footer clearfix">
      <span id="img_vss" class="display-none vss"><i  class="fa fa-spinner fa-pulse"></i></span>
      <form action="<?=$script;?>" method="post">
          <input type="hidden" name="action" id="img_crop_action"/>
          <input type="hidden" name="subject" id="img_crop_subject" value="<?=$_SERVER['QUERY_STRING'];?>"/>
          <input type="hidden" name="base64" id="img_base64" />
          <button id="modal_btn_crop" title="crop" type="button"><span class="small">crop</span><i class="fa fa-crop float-right"></i></button>
      </form>
      <button id="modal_btn_close_crop" title="cancel"><span class="small">cancel</span><i class="fa fa-close float-right"></i></button>
    </div>
  </div>
</div>
<style>
.modal {position: fixed;z-index: 10;padding-top: 1%;left: 0;top: 0;width: 100%;height: 100%;overflow: auto;background-color: rgba(0,0,0,0.6);}
.modal-content {position: relative;margin: auto;padding: 0;border: 1px solid #888;width: 100%;box-shadow: 0 4px 8px 0 rgba(0,0,0,0.2),0 6px 20px 0 rgba(0,0,0,0.19);-webkit-animation-name: portal;-webkit-animation-duration: 0.7s;animation-name: portal;animation-duration: 0.7s;}
@-webkit-keyframes portal {from {width: 100%;opacity:0}   to {width: 80%;opacity:1}}
@keyframes portal { from {width: 100%;opacity:0} to {width: 80%;opacity:1}}
#modal_img_title{text-align: center;color: var(--bg-color)}
#crop_space{overflow: auto}
.crop-space .cr-slider-wrap {width: 100%;position: absolute;top: 0;left: 0;z-index: 1;}
.crop-space .cr-slider {background-color: #fff3;}
#modal_img .img-rotate {cursor: pointer;top: 40%;z-index: 3;}
#modal_img .img-rotate .fa , #img_vss{font-size: 36px;color: var(--yin);text-shadow: 0 3px 3px rgba(20,20,20,.4);}
#modal_img .modal-footer button {width: 60%;margin: 0 auto;display: block;background-color: var(--dark-color);color: var(--yin);border: 2px solid var(--yin);cursor: pointer;padding: 9px;border-width: 0px 2px;text-align: left;}
.modal-footer button i{font-size: 28px}
#modal_btn_crop:hover *{color: #0f6;}
#modal_btn_close_crop:hover *{color: #f80;}

@media screen and (min-width:768px) {
  #modal_btn_crop,#img_vss{float: right;}
  #modal_btn_close_crop{float: left;}
  #modal_img .modal-header, .modal-footer{padding: 2px 16px}
 #modal_img .modal-content{width: 80%}
  #modal_img .modal-footer button {width: initial;border-width: 2px}
}
</style>
<script>

</script>
