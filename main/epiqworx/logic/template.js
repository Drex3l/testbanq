function img_preview(source,display){

  var reader = new FileReader();
  reader.onload = function(){   display.src = reader.result;}

  if(source.files[0]) reader.readAsDataURL(source.files[0]);
  else display.src = null;
}
function $$(selector,context){
  context = context || document;
  var elements = context.querySelectorAll(selector);
  return Array.prototype.slice.call(elements);
}

/* display [object:panel_1], and add class [string:classname]
* hide [object:panel_2] and remove class [string:classname]
*/
function switch_panels(panel_1,panel_2,boogeyman){
  if(typeof boogeyman !== "string" || boogeyman.trim().length < 1) {
    boogeyman = 'display-none';
  }
  panel_2.classList.add(boogeyman);
  panel_1.classList.remove(boogeyman);
}

/* ensure the correct radio button is selected on load
*/
// radio button
function radio_select(name,state){
  var rbn = document.getElementsByName(name);
  for (var i = rbn.length - 1; i >= 0; i--) {
    if(rbn[i].value === state) rbn[i].checked = true;
  }
}
function radio_valid(target,report_error,err_msg) {
    let ans_rbn = document.getElementsByName(target);
    for (let i = 0; i < ans_rbn.length; i++) if (ans_rbn[i].checked) return true;
    for (let i = 0; i < ans_rbn.length; i++) report_error(ans_rbn[i],err_msg);
    return false;
}
function radio_value(target){
    let ans_rbn = document.getElementsByName(target);
    for(let i = 0 ; i < ans_rbn.length ; i++) if(ans_rbn[i].checked) return ans_rbn[i].value;
    return '-1';
}
// select element
function select_option(select,data,id,natural){
    let opt = document.createElement('option');
    opt.value = data['VALUE'];
    opt.innerHTML = data['DISPLAY'];
    if(typeof id === 'string' && id.length > 0) opt.id = 'opt_' + id + '_' + data['VALUE'];
    
    if(typeof natural === 'string' && natural.length > 0) if (data['VALUE'] === natural) opt.selected = true;
    
    select.appendChild(opt);
}
// table element
function table_data_empty(table,message,classlist){
    let cell = document.createElement('td');
    cell.innerHTML = message;
    cell.classList = 'align-center ls-empty';

    let row = document.createElement('tr');
    if(typeof classlist === 'string' && classlist.length > 0) row.classList = classlist;
    row.appendChild(cell);
    table.appendChild(row);
}
function download(data, filename, type) {
  var file = new Blob([data], {type: type});
  if (window.navigator.msSaveOrOpenBlob) // IE10+
  window.navigator.msSaveOrOpenBlob(file, filename);
  else { // Others
    var a = document.createElement("a"),
    url = URL.createObjectURL(file);
    a.href = url;
    a.download = filename;
    document.body.appendChild(a);
    a.click();
    setTimeout(function() {
      document.body.removeChild(a);
      window.URL.revokeObjectURL(url);
    }, 0);
  }
}
