
function upload_img(source,space,rm,x,y,resize,rotate_key,title,callback){
  var rotate = (typeof rotate_key === 'string' && rotate_key.length > 0);
  var crop = {};

  if(resize) crop = {viewport:{width:x,height:y},enableOrientation: rotate,enableResize: resize,boundary:{width: parseInt(x + (x * .4)),height:y+40}};
  else crop = {viewport:{width:x,height:y,enforceBoundary:false, type: 'square'},enableOrientation: rotate,boundary:{width: parseInt(x + (x * .4)),height:y}};

  // // instantiate croppie object 'c'
  c = new Croppie(space, crop);
  if(rotate){
    // add rotate functionality to rotate keys
    var btn_rotate = document.getElementsByClassName(rotate_key);
    for(i = 0 ; i < btn_rotate.length ; i++) btn_rotate[i].onclick = function(){c.rotate(parseInt(this.title));};
  }
  // // read selected file and bind its url to croppie object
  if (source.files && source.files[0]) {
    var reader = new FileReader();
    reader.onload = function (e) {
      c.bind({url: e.target.result,orientation: 1}).then(callback);
    }
    reader.readAsDataURL(source.files[0]);
  }
  // // switch icon display panel with that of cropping an image
  if(rm !== null)  switch_panels(panel_crop_space,rm);
  // set modal modal title
  if(typeof title === 'string' && title.length > 0) modal_img_title.innerHTML = title;
  return c;
}
function discard_croppie(c){
  var btn_rotate = document.getElementsByClassName('img-rotate');
  for(i = 0 ; i < btn_rotate.length ; i++) btn_rotate[i].onclick = null;
  c.destroy();
}
function crop_blob(){
  c.result('blob').then(function(blob) {
    window.URL = window.URL || window.webkitURL;
    instance_icon_display.src = window.URL.createObjectURL(blob);
  });
}
function crop_canvas(){
  c.result('canvas').then(function(canvas){
    // instance_icon_display.src = canvas;
    // repo_icon_display.style.opacity = "1";
    window.open(canvas);
    // switch_panels(panel_img,panel_crop_space);
  }).catch(function(ex){window.alert(ex.message);});
}
function this_close_crop()
{
	var panel = document.getElementById(btn_close_this_crop.name.split('-')[0]);	// panel to return when removing croppie panel
  document.getElementById(btn_close_this_crop.name.split('-')[1]).value = null;	//	cleare 'file' input element
	document.getElementById('imagebase64_'+btn_close_this_crop.name.split('-')[1]).value = null;	// clear image base64 blob hidden field
	switch_panels(panel,panel_crop_space);	// swap panels
}
