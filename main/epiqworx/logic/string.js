//<editor-fold desc="Text Validation" defaultstate="collapsed">
function is_int(value) {
  var text = anti_whitespace(value);
  for (let k = 0; k < text.length; k++)
  {
    if (text.charCodeAt(k) < 48 || text.charCodeAt(k) > 57)
    return false;
  }
  return true;
}
function anti_whitespace(text) {
  var txt = "";
  for (let k = 0; k < text.length; k++)
  {
    if (text.charCodeAt(k) > 32)
    txt += text.charAt(k);
  }
  return txt;
}
function is_name(txt){
  var exp1 = /^(([A-Za-z]+[\-\']?)*([A-Za-z]+)?\s)+([A-Za-z]+[\-\']?)*([A-Za-z]+)?$/;
  var exp2 = /^[a-z\u00C0-\u02AB'´`]+\.?\s([a-z\u00C0-\u02AB'´`]+\.?\s?)+$/i;
  var exp3 = /^[A-Za-z\'\s\.\,]+$/;
  var exp4 = /^\w(\w|\s|['.])*$/;

  var regex = new RegExp(exp3);
  if (txt.match(regex))
  return true;
  else
  return false;
}
function is_name_plus(txt,min,max){
  txt = txt.toString().trim();
  if(txt.length<min) return "length below "+min+" chars";
  if(txt.length>max) return "length above "+max+" chars";

  if(!txt.match(/^[A-Za-z\'\s\.\,]+$/))   return "name invalid";

  return "success";
}
function is_alphabet(value) {
  var text = anti_whitespace(value);
  text = text.toUpperCase();
  for (let k = 0; k < text.length; k++)
  {
    if (text.charCodeAt(k) < 65 || text.charCodeAt(k) > 90)
    return false;
  }
  return true;
}
function is_symbol(char){
  char = anti_whitespace(char);
  if (char.length === 1)
  {
    if (char.charCodeAt(0) < 48 || (char.charCodeAt(0) > 57 && char.charCodeAt(0) < 65) || (char.charCodeAt(0) > 90 && char.charCodeAt(0) < 97) || char.charCodeAt(0) > 122)
    return true;
  }
  if (char.length > 1)
  {
    for (let k = 0; k < char.length; k++)
    if (char.charCodeAt(k) < 48 || (char.charCodeAt(k) > 57 && char.charCodeAt(k) < 65) || (char.charCodeAt(k) > 90 && char.charCodeAt(k) < 97) || char.charCodeAt(k) > 122)
    return true;
  }
  return false;
}
function is_valid_key(name, list) {
  for (let k = 0; k < list.length; k++) {
    if (name.toUpperCase() === list[k].toUpperCase()) return false;
  }
  return true;
}
function is_email(txt){
  txt = txt.toString().trim();
  var mailformat = /^\w+([\.-]?\w+)*@\w+([\.-]?\w+)*(\.\w{2,3})+$/;   //------email regex
  if (txt.match(mailformat)) return true;
  return false;
}
function is_url(txt){
  var exp1 = /[-a-zA-Z0-9@:%_\+.~#?&//=]{2,256}\.[a-z]{2,4}\b(\/[-a-zA-Z0-9@:%_\+.~#?&//=]*)?/gi;
  var exp2 = /(https?:\/\/(?:www\.|(?!www))[a-zA-Z0-9][a-zA-Z0-9-]+[a-zA-Z0-9]\.[^\s]{2,}|www\.[a-zA-Z0-9][a-zA-Z0-9-]+[a-zA-Z0-9]\.[^\s]{2,}|https?:\/\/(?:www\.|(?!www))[a-zA-Z0-9]\.[^\s]{2,}|www\.[a-zA-Z0-9]\.[^\s]{2,})/;
  var exp3 = "^(http[s]?:\\/\\/(www\\.)?|ftp:\\/\\/(www\\.)?|www\\.){1}([0-9A-Za-z-\\.@:%_\+~#=]+)+((\\.[a-zA-Z]{2,3})+)(/(.)*)?(\\?(.)*)?";

  if (txt.toLowerCase().match(exp3))
  return true;
  else
  return false;
}
function is_st_address(txt){
  var exp1 = /^[a-zA-Z\s\d\/]*\d[a-zA-Z\s\d\/]*$/;
  var exp2 = /^\d+\s[A-z]+\s[A-z]+/g;

  var regex = new RegExp(exp1);
  if (txt.match(regex))
  return true;
  else
  return false;
}
//</editor-fold>
//<editor-fold desc="CSV Operations" defaultstate="collapsed">
function csv_rows(data,d){
  var line = data.toString().split('\n'); // split data by rows
  let length = line.length;

  if(!(typeof d === "string" && d.length > 0)) d = ';';
  if(!line[length-1].includes(d)) length--;
  var records = [];
  for (let i = 0; i < length; i++)  records.push(line[i].split(d));      // split record into fields

  return records;
}
function csv_cols(data,index,d){
  let line = data.toString().split('\n'); // split data by rows
  let length = line.length;

  if(!(typeof d === "string" && d.length > 0)) d = ';';
  if(!line[length-1].includes(d)) length--;
  var cols = [];
  for(let y = 0;y < length; y++) cols.push(line[y].split(d)[index]);
  return cols;
}
function csv_query(data,d,key,value){
  var line = data.toString().split('\n'); // split data by rows
  let length = line.length;

  if(!(typeof d === "string" && d.length > 0)) d = ';';
  if(!line[length-1].includes(d)) length--;
  var records = [];
  for(let i = 0;i < length; i++){
    var record = line[i].split(d);
    if(record[key].toUpperCase() === value.toUpperCase()) records.push(record);
  }
  return records;
}
function arr_cols(data,index){
  var cols = [];
  for(let y = 0;y < data.length; y++) cols.push(data[y][index]);
  return cols;
}
//</editor-fold>
//<editor-fold desc="Text Padding" defaultstate="collapsed">
function pad_left(nr, n, str) {
  return Array(n - String(nr).length + 1).join(str || '0') + nr;
}
//or as a Number prototype method:
/*
* examples
* console.log(padLeft(23,5));
* => '00023'
*/
//+negetive number
Number.prototype.pad_left = function (n, str) {
  return (this < 0 ? '-' : ' ') + Array(n - String(Math.abs(this)).length + 1).join(str || '0') + (Math.abs(this));
};
/*
* console.log((-23).padLeft(5)); => '-00023'
*/
function padder(len, pad) {
  if (len === undefined) {
    len = 1;
  } else if (pad === undefined) {
    pad = '0';
  }
  var pads = ' ';
  while (pads.length < len) {
    pads += pad;
  }
  this.pad = function (what) {
    var s = what.toString();
  };
}
/*
* var zero4 = new Padder(4);
* zero4.pad(12);   // "0012"
* zero4.pad(12345);     // "12345"
* zero4.pad("xx");     // "00xx"
* var x3 = new Padder(3,"x");
* x3.pad(12);  // "x12"
*/
//</editor-fold>
