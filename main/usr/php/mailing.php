<?php
use PHPMailer\PHPMailer\PHPMailer;
use PHPMailer\PHPMailer\Exception;

require dirname(__FILE__, 2) . '/library/phpmailer/src/Exception.php';
require dirname(__FILE__, 2) . '/library/phpmailer/src/PHPMailer.php';
require dirname(__FILE__, 2) . '/library/phpmailer/src/SMTP.php';

abstract class Mailing {
    private static $cs_file = "connection.ini";
    private static $cs_path = "config/";

    public static function send_mail($subject, $message, $recipient) {
        $cs_path = dirname(__FILE__, 3). '/'.self::$cs_path;
        $cs_file = $cs_path.self::$cs_file;
        $conexion = parse_ini_file($cs_file);
        
        $mail = new PHPMailer(true);
        $mail->IsSMTP();
        try {
            $mail->Host = $conexion['smpt_host'];

//enable SMTP
            $mail->isSMTP();
//            $mail->SMTPDebug = 2;
            $mail->CharSet = 'UTF-8';

//set authentication to true
            $mail->SMTPAuth = true;

//set login details for email account
            $mail->Username = $conexion['email_acc'];
            $mail->Password = $conexion['email_pass'];

//set type of protection
            $mail->SMTPSecure = $conexion['smpt_secure']; //or we can use TLS
//set a port
            $mail->Port = $conexion['smtp_port'];      //or 465 if SSL
//set subject
            $mail->Subject = $subject;

//set HTML to true
            $mail->isHTML(true);

//set body
            $mail->Body = $message;

//set who is sending an email$message
            $mail->setFrom($conexion['email_acc'], $conexion['email_sender']);

//set where we are sending email (recipents)
            if (empty($recipient)) {
                return "-4";
            }
            $mail->addAddress($recipient);

//send an email
            if (!$mail->send()) {
                return $mail->ErrorInfo;
            } else {
                return "0";
            }
        } catch (phpmailerException $e) {
            return $e->getMessage();
        } catch (Exception $e) {
            return $e->getMessage();
        }
    }

    public static function read_file($file, $id, $recipient, $token) {
        $mailfile = dirname(__FILE__,3) . "/usr/php/email/$file";
        if (!file_exists($mailfile)) {
            return "-1";
        }
        $handle = fopen($mailfile, "r");

        if ($handle) {
            $k = 0;
            $message = "";
            while ((($buffer = fgets($handle, 4096)) !== false) && ($k < 24)) {
                $message .= $buffer;
                $k++;
            }
            if (!feof($handle)) {
            }
            $action = explode('-',explode(".", $file)[0])[1];

            switch ($action) {
                case "account":
                    $message .= "<a href='http://" . filter_input(INPUT_SERVER, 'HTTP_HOST') . "/testbank/main/usr/php/ajax/profile.php?action=activate&lecturer&id=$id&hash=$token' style='margin-left:2em;background-color: #444;color: #d5d5d5;text-decoration: none;width: 85px;line-height: 30px;display: block;border-radius: 40px;border: 1px solid #444;transition: 1s;text-align: center;font-family: Arial, Sans-serif' >Lecturer</a>";
                    break;
                case "email":
                    $message .= "<a href='http://" . filter_input(INPUT_SERVER, 'HTTP_HOST') . "/testbank/main/usr/php/ajax/profile.php?action=reset&email&id=$id&new=$recipient&hash=$token' style='margin-left:2em;background-color: #444;color: #d5d5d5;text-decoration: none;width: 85px;line-height: 30px;display: block;border-radius: 40px;border: 1px solid #444;transition: 1s;text-align: center;font-family: Arial, Sans-serif' >Reset</a>";
                    break;
                case "passwd":
                    $message .= "<a href='http://" . filter_input(INPUT_SERVER, 'HTTP_HOST') . "/testbank/main/usr/php/ajax/profile.php?action=reset&passwd&id=$id&hash=$token' style='margin-left:2em;background-color: #444;color: #d5d5d5;text-decoration: none;width: 85px;line-height: 30px;display: block;border-radius: 40px;border: 1px solid #444;transition: 1s;text-align: center;font-family: Arial, Sans-serif' >Reset</a>";
                    break;
            }
            if ($action === "account") {
                while ((($buffer = fgets($handle, 4096)) !== false) && ($k < 28)) {
                    $message .= $buffer;
                    $k++;
                }
                $message .= "<a href='http://" . filter_input(INPUT_SERVER, 'HTTP_HOST') . "/testbank/main/usr/php/ajax/profile.php?action=activate&student&id=$id&hash=$token' style='margin-left:2em;background-color: #737373;color: #d5d5d5;text-decoration: none;width: 85px;line-height: 30px;display: block;border-radius: 40px;border: 1px solid #444;transition: 1s;text-align: center;font-family: Arial, Sans-serif' >Student</a>";
            }

            while (($buffer = fgets($handle, 4096)) !== false) {
                $message .= $buffer;
                $k++;
            }
            if (!feof($handle)) {
            }
            fclose($handle);
        } else {
            return "-2";
        }
        return $message;
    }
}
