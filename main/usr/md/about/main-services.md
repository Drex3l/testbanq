* ### question data bank
Questions created by various lecturers for various modules are stored in a central databank.
These questions can be then grouped by specific categories.

* ### student feedback system
System allows students to track their performance with tests taken via testbank.
Automated marking allows instant feedback to student via account on previously written test.

* ### elaborate search engine
Engine allows querying of question data bank for quick access.
Filtering controls allows for specifics based shortlisting. 

* ### community networks
Users can associate with others of common interest, and also create/join user groups, giving them access to a multitude of resources.