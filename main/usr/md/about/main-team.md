# Who is the Sanhedrin?{.flash}
![logo](main/usr/img/user/dev/compact-sanhedrin.png){.flash}

Somethings just must start somewhere. This is amply evident with giant technology corporations of today, who commenced from garages.

The **Sanhedrin** is a collaboration of two repeat students, _School of ICT_, _Nelson Mandela University_, in the express purpose of bringing about the wonders of the **projects** into existence, thus forming an alliance.

What future does this concept organisation hold for the success of the testbanq project? ...only time will tell

Doors to join forces with the Sanhedrin are still open. In the meantime, meet the current members as follows...

* ## 214133176{.flash}
    ![avatar](main/usr/img/user/dev/214133176.png){.flash}
    Meet **Chuma Gantsho**, otherwise aliased _"ice"_, the lead system analysis and design, requirements gatherer, and usability officer, in the testbanq project.
    #### E
    vidently sir Gantsho has many talents, as can be discerned from the above. Most notable is his business skills and understanding of market psychology, ultimately putting him in the slot of **project leader** in the project.  
    This instance alone should paint you a picture of where the testbanq is heading, as a project.  
    Another significant role sir Gantsho has with the project is that of designing the graphical user interface, as he is also in charge of this department.

* ## 215013395{.flash}
    ![avatar](main/usr/img/user/dev/215013395.png){.flash}
    #### D
    **aluxolo Msikinya** is the engineering behind the project. Concerns are raised that this individual is not known to have accomplished anything tangible in his lifetime so far, as far as **computer programming** is concerned;
    why put him in such a sensitive role? Who is this guy?  
    Some argue that sir Msikinya is the kind of extrovert that you just simply lock up in the server room, and throw away the keys, for he sucks at convincing anyone of anything.  
    >However, the continued argument is that he supplements this scientific shortfall with his ingenuity to at least convince code to compile effective executables.
