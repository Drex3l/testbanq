function sideline(IUD,print){
	// IUD - Insert * Update * Delete
	var container = document.getElementsByClassName('group-I');		// IUD button containers
	var button = document.getElementsByClassName('sideline-btn');	// IUD buttons
	var shield = document.getElementsByClassName('btn-shield');		// container to shield buttons
	for(i=0;i<button.length;i++)
	{
		switch(IUD[i]){
			case '1':	// activate button
			shield[i].classList.add('display-none');
			container[i].classList.remove('off');
			break;
			case '0':	// deactive button
			shield[i].classList.remove('display-none');
			container[i].classList.add('off');
			break;
			default:
			window.alert('\''+IUD[i]+'\' not understood');
			return;
			break;
		}
	}
	if(print) sideline_status.value = IUD;
}
function err_field(field,txt){
	var cmc = document.getElementById("cmc_"+field.id.substr(field.id.length-2,2));	//	create cmc element to display error message
	//	reseting elements
	cmc.classList.remove("err");
	cmc.innerHTML = cmc.title;
	field.classList.remove('err');

	if(typeof txt === "string" && txt.length > 0) {
		cmc.classList.add("err");
		field.classList.add('err');
		cmc.innerHTML = txt;	// displaying error message
	}
}
function folding(cmp) {
    cmp.classList.toggle("active");
    let content = cmp.nextElementSibling;
    if (content.style.maxHeight){
      content.style.maxHeight = null;
    } else {
      content.style.maxHeight = content.scrollHeight + "px";
    } 
  }
//<editor-fold desc="navigation" defaultstate="collapsed">
function crumbplus(aug,label,section){
    let butter = [document.createElement('a'),document.createElement('a')];
    butter[0].innerHTML = butter[1].innerHTML = label;

    let slice = [document.createElement('li'),document.createElement('li')];
    slice[0].classList = 'crumb+';
    slice[0].id = 'l' + aug.length + '_slice';
    slice[0].appendChild(butter[0]);
    slice[0].setAttribute('section',section);
    
    slice[1].id = 'bn_l'+ aug.length;
    slice[1].appendChild(butter[1]);

    m_nav_crumb.appendChild(slice[0]);
    bottom_nav_crumb.appendChild(slice[1]);
}
function crumbsection(){
    let aug = document.getElementsByClassName('crumb+');
    return aug[aug.length-1].getAttribute('section');
}
m_nav_key.addEventListener('click',function(e){
	menu_nav.classList.toggle('big-block');
	menu_nav.classList.toggle('base-dotted-dark');
});

m_tbl_osd_toggle.addEventListener('click',function(e){
	m_tbl_osd_toggle.classList.toggle('active');
	if(m_tbl_osd_toggle.classList.contains('active')){
		m_tbl_osd.classList.remove('display-none');
	}else{
		m_tbl_osd.classList.add('display-none');
	}
});
//</editor-fold>