function load() {
    window.sessionStorage.clear();
	/* containers containing javascript content are hidden by default
	* the following lines of code removes the css class hiding them
	*/
	var container = document.getElementsByClassName('js');
	for (var i = container.length - 1; i >= 0; i--) {
		container[i].classList.remove('js');
	}
	if(url_exists(window.localStorage.item4)){
            get_ajax(window.localStorage.item4+'?action=ls&subject=this',function(data){prepare_list(data,instance_list,'prime');});  // prepare list of instances (repos)
            get_ajax(window.localStorage.item4 +'?action=ls&subject=popularity-contest',function(reply){
                let data = JSON.parse(reply);
                //  group select
                populate_select_group_sec_preview_p2(data['GROUPS']);
                //  repo select
                populate_select_repo_sec_preview_p2(data['REPOS']);
            });
            get_ajax(window.localStorage.item4 + '?action=count&subject=this',function(stats){
                m_osd_1.innerHTML = osd_1.innerHTML = stats;
            });
        }
	else {
		instance_h1.innerHTML = 'Datasource Error';
		instance_p1.innerHTML = 'Item list can\'t be rendered';
		instance_p2.innerHTML = 'Address the following anormally;-';
		instance_err.innerHTML = 'file \''+ localStorage.item4 +'\' missing!';
		return;
	}
	sideline('000',true);	// active button on the left sideline to add instance
}
document.body.onload = load;
document.body.addEventListener('keypress',function(e){
	switch(e.key){
		case 'F1':
		console.log('mayday!');
		break;
		case 'F4':
		case 'F8':
		case 'F9':
		test(e.key);
		break;
		default:
		// console.log(e.key);
		break;
	}
});
function prepare_list(data,parent,target){
    let record = JSON.parse(data);
    if (record.length < 1) {
        var li = document.createElement('li');
        li.innerHTML = 'list empty';
        li.classList = target + '-instance-zero';
        parent.appendChild(li);
    } else if (typeof record === 'object' && !Array.isArray(record)) {
        present_instance(record, parent);
    } else {
        for (var i = 0; i < record.length; i++) {
            present_instance(record[i], parent);
        }
    }
}
function present_instance(instance,parent){
    // instance name
	var span_name = document.createElement('span');
	span_name.classList.add('list-title-text');
	span_name.innerHTML = instance['STAT_NAME'];
        
        // instance description
	let desc = '';
        if(typeof instance['STAT_DESC'] === 'string' && instance['STAT_DESC'].length > 0){
            desc = instance['STAT_DESC'].replace('\\xd\\xa',' ').replace('/',' ');
            desc = desc.replace('/',' ');
            if(desc.length > 32) desc = desc.substr(0,32)+ '...';
        }
	var span_desc = document.createElement('span');
	span_desc.classList.add('list-desc-text');
	span_desc.innerHTML = desc;

	// instance [name + description] container
	var text = document.createElement('h1');
	text.appendChild(span_name);
	text.appendChild(span_desc);
        
        // instance text ultimate container
	var text_div = document.createElement('div');
	text_div.classList.add('flex-column');
	text_div.classList.add('list-body');
	text_div.appendChild(text);
        
        // instance 1st generation child
	var container = document.createElement('div');
	container.classList.add('flex-row');
//	container.appendChild(img);
	container.appendChild(text_div);
        
        var li = document.createElement('li');
        li.addEventListener('click',function(e){instance_select(this,instance);});
	li.appendChild(container);
        li.setAttribute('data-id',instance['STAT_ID']);
        li.title =  instance['STAT_DESC'];
	parent.appendChild(li);
}

function instance_select(ctrl,instance){
    //  if not already selected,make server request
    if (!ctrl.classList.contains('instance-active')) {
        switch (instance['STAT_ID']) {
            case 'PC0':panel_report.classList.remove('display-none');
                get_ajax(window.localStorage.item4 +
                        '?action=present&subject=popularity-contest&a=' + plc_start_date.value + '&b=' + plc_end_date.value + '&target=' + cbx_foucus_action.value + '&repo=' + select_repo_sec_preview_p2.value,
                        function (data) {
                            switch_panels(panel_controls_pc, panel_controls_ri);
                            popularity_contest(JSON.parse(data));
                        });
                break;
            case 'RI0':panel_report.classList.add('display-none');
                get_ajax(window.localStorage.item4 +
                        '?action=present&subject=repo-intensity&gid=' + select_group_sec_preview_ri.value ,
                        function (data) {console.log(data);
                            switch_panels(panel_controls_ri, panel_controls_pc);
//                            popularity_contest(JSON.parse(data));
                        });
                break;
        }
    }
    // change instance selection to active
    let cmp = document.getElementsByClassName('instance-active');
    for (i = 0; i < cmp.length; i++) cmp[i].classList.remove('instance-active');
    ctrl.classList.add('instance-active');
    
    _btnback.classList.remove('display-none');
    btn_back.classList.remove('display-none');
    // mobile navigation breadcrumb
    let aug = document.getElementsByClassName('crumb+');
    if(aug.length < 2){
        if(aug.length > 1) {
            m_nav_crumb.removeChild(m_nav_crumb.lastChild);
            bottom_nav_crumb.removeChild(bottom_nav_crumb.lastChild);
        } // ensures 2nd slice on bread
        //  accumulate breadcrumb with current panel
        crumbplus(aug,instance['STAT_NAME'].split(' ')[0]);
        // mobile view navigation
        m_btn_dml.classList.add('shift');
        m_panel_dml.classList.add('shift');
        m_btn_back_to_ls.classList.remove('display-none');
        list_panel.classList.add('x1024');
        switch_panels(panel_report,panel_default);
        panel_controls_pc.classList.remove('display-none');
    }else {
        bn_l1.innerHTML = null;
        let repo_slice = document.createElement('a');
        repo_slice.innerHTML= instance['STAT_NAME'].split(' ')[0];
        bn_l1.appendChild(repo_slice);
    }
}
//<editor-fold desc="cell 2 functionality" defaultstate="collapsed">
function popularity_contest(report){
    get_ajax(window.localStorage.item1 + '?action=ls&target=this-quest&id='+select_repo_sec_preview_p2.value,function(data){
        populate_tbl_ls_quest(JSON.parse(data),true);
    });
    let labels = [];
    let series = [];
    
    for (let y = 0; y < report.length; y++) {
        series[y] = [];
        for (let x = 0; x < report[y]['DATA'].length; x++) {
            labels[x] = report[y]['DATA'][x]['DAY'].split('-')[1]+'/'+report[y]['DATA'][x]['DAY'].split('-')[2];
            series[y][x]= report[y]['DATA'][x]['HITS'];
        }
    }
    let set = {labels: labels,series: series};
    
    let options = {width: 480,height: 360,onlyInteger:true,stretch:true};
    new Chartist.Line('.ct-chart', set, options);
}
function repo_intensity(report){
    let labels = [];
    let series = [];
    
    
    let set = {labels: labels,series: series};
    let options = {width: 480,height: 360,onlyInteger:true,stretch:true};
    new Chartist.Line('.ct-chart', set, options);
}
//</editor-fold>
//<editor-fold desc="cell 3 functionality" defaultstate="collapsed">
function build_quest_ls(quest,checked,index){
    if (quest['READY'] !== '1') return;// return complete questions only, on remote repos
    if(typeof checked !== 'boolean') checked = false;
    
    let line = 'ct-series-' + String.fromCharCode(97+index);
    let input = document.createElement('input');
    input.type = 'checkbox';
    input.classList = 'float-right';
    input.checked = checked;
    input.id = 'cbx_quest_' + quest['ID'];
    input.setAttribute('stroke',line);
    input.onchange = function(e){
        let target = document.getElementsByClassName(line);
        if(this.checked){
            target[0].classList.remove('display-none');
        }else{
            target[0].classList.add('display-none');
        }
    };
    
    let qtype = document.createElement('label');
    qtype.classList = 'display-block cursor-pointer';
    
    qtype.appendChild(document.createTextNode(quest['QTYPE_DESC']));
    qtype.appendChild(input);
    
    let qtxt = quest['QUESTION'];
    let txtlen = 32;
    if(typeof qtxt === 'string' && qtxt.trim().length > txtlen) qtxt = qtxt.substr(0, txtlen) + '...';
    let cell = [document.createElement('td'),document.createElement('td')];
    
    cell[0].innerHTML  = quest['CREATED'];
    cell[0].classList = 'extra';
    
    cell[1].classList = 'clearfix align-left';
    cell[1].appendChild(qtype);
    
    let tr = document.createElement('tr');
    tr.title = quest['QUESTION'];
    for(let i = 0 ; i < cell.length ; i++) tr.appendChild(cell[i]);
    tbl_ls_quest.appendChild(tr);
}
function populate_tbl_ls_quest(data,checked){
    tbl_ls_quest.innerHTML = null;
    if(data.length < 1) {}
    else if(typeof data === 'object' && !Array.isArray(data))build_quest_ls(data,checked,0);
    else data.forEach(function(data,index){build_quest_ls(data,checked,index);});
}
//<editor-fold desc="Event Handling" defaultstate="collapsed">
function populate_select_repo_sec_preview_p2(data){
    select_repo_sec_preview_p2.innerHTML = null;
    if(data.length < 1) select_option(select_repo_sec_preview_p2,{'DISPLAY':'--','VALUE':'-1'},'group');
    else if(typeof data === 'object' && !Array.isArray(data))select_option(select_repo_sec_preview_p2,{'DISPLAY':data['REPO_NAME'],'VALUE':data['REPO_ID']},'repo');
    else for(let i = 0 ; i < data.length ; i++) select_option(select_repo_sec_preview_p2,{'DISPLAY':data[i]['REPO_NAME'],'VALUE':data[i]['REPO_ID']},'repo');
}
select_repo_sec_preview_p2.onchange = function(e){
    get_ajax(window.localStorage.item1 + '?action=ls&target=this-quest&id='+this.value,function(data){
        populate_tbl_ls_quest(JSON.parse(data));
    });
    get_ajax(window.localStorage.item4 + 
            '?action=present&subject=popularity-contest&a='+plc_start_date.value +'&b='+plc_end_date.value + '&target='+cbx_foucus_action.value + '&repo='+this.value,
    function(data){
        popularity_contest(JSON.parse(data));
    });
}

function populate_select_group_sec_preview_p2(data){
    select_group_sec_preview_p2.innerHTML = select_group_sec_preview_ri.innerHTML = null;
    select_option(select_group_sec_preview_p2,{'DISPLAY':'local','VALUE':'0'},'group_pc','0');
    select_option(select_group_sec_preview_ri,{'DISPLAY':'local','VALUE':'0'},'group_ri','0');
    if(data.length < 1) {}
    else if(typeof data === 'object' && !Array.isArray(data)){
        select_option(select_group_sec_preview_p2,{'DISPLAY':data['GROUP_NAME'],'VALUE':data['GROUP_ID']},'group_pc');
        select_option(select_group_sec_preview_ri,{'DISPLAY':data['GROUP_NAME'],'VALUE':data['GROUP_ID']},'group_ri');
    }
    else for(let i = 0 ; i < data.length ; i++) {
        select_option(select_group_sec_preview_p2,{'DISPLAY':data[i]['GROUP_NAME'],'VALUE':data[i]['GROUP_ID']},'group_pc');
        select_option(select_group_sec_preview_ri,{'DISPLAY':data[i]['GROUP_NAME'],'VALUE':data[i]['GROUP_ID']},'group_ri');
    }
}
select_group_sec_preview_p2.onchange = function(e){
    switch(this.value){
        case '0':
            get_ajax(window.localStorage.item1 + '?action=ls&target=this', function (data) {
                populate_select_repo_sec_preview_p2(JSON.parse(data));
            });
        break;
        default:
            get_ajax(window.localStorage.profile + '?action=group-repos&gid=' + this.value, function (data) {
                populate_select_repo_sec_preview_p2(JSON.parse(data));
            });
            break;
    }
}

cbx_foucus_action.onchange = function(e){
    get_ajax(window.localStorage.item4 + 
            '?action=present&subject=popularity-contest&a='+plc_start_date.value +'&b='+plc_end_date.value + '&target='+this.value + '&repo='+select_repo_sec_preview_p2.value,
    function(data){
        popularity_contest(JSON.parse(data));
    });
}
plc_start_date.oninput = function(e){console.log(this.value);
    get_ajax(window.localStorage.item4 + 
            '?action=present&subject=popularity-contest&a='+this.value +'&b='+plc_end_date.value + '&target='+cbx_foucus_action.value + '&repo='+select_repo_sec_preview_p2.value,
    function(data){
        popularity_contest(JSON.parse(data));
    });
}
plc_end_date.oninput = function(e){console.log(this.value);
    get_ajax(window.localStorage.item4 + 
            '?action=present&subject=popularity-contest&a='+plc_start_date.value +'&b='+this.value + '&target='+cbx_foucus_action.value + '&repo='+select_repo_sec_preview_p2.value,
    function(data){
        popularity_contest(JSON.parse(data));
    });
}
//</editor-fold>
//</editor-fold>
//<editor-fold desc="navigation" defaultstate="collapsed">
function sec_preview_nav(cmp){
    if(cmp.classList.contains('active'))return;
    //  switch selection
    let key = document.getElementsByClassName('section-preview-nav');
    for(let i = 0 ; i < key.length ; i++) key[i].classList.remove('active');
    cmp.classList.add('active');
    switch(cmp.id){
        case 'sec_preview_p1':
            switch_panels(panel_sec_preview_p1,panel_sec_preview_p2);
            break;
        case 'sec_preview_p2':
            switch_panels(panel_sec_preview_p2,panel_sec_preview_p1);
            break;
    }
}
function m_crumb_nav() {
    let crumb = document.getElementsByClassName('crumb+');
    let cmp = document.getElementsByClassName('instance-active');
    
    switch (crumb.length) {
        case 2:
            purge_panels();
            panel_default.classList.remove('display-none');
            // change instance selection to active
            for (i = 0; i < cmp.length; i++) cmp[i].classList.remove('instance-active');
            // reset mobile keys
            m_btn_dml.classList.remove('shift');
            m_panel_dml.classList.remove('shift');
            m_btn_back_to_ls.classList.add('display-none');
            list_panel.classList.remove('x1024');
            _btnback.classList.add('display-none');
            btn_back.classList.add('display-none');
            break;
            }
    m_nav_crumb.removeChild(m_nav_crumb.lastChild);
    bottom_nav_crumb.removeChild(bottom_nav_crumb.lastChild);
}
function purge_panels(boogeyman) {
	sideline('000',false);	// activate * buttons on sideline
	if(typeof boogeyman !== "string" || boogeyman.trim().length < 1) boogeyman = 'display-none';
	//  -------------------------------------- remove Instance Select panel
        /* The Dash Has 3 Cells
         * cell 1 list instances of current tab item
         * cell 2 is the main functionality content on the selected instance
         * cell 3 is the aside panel, with additional functionality
         */
        // cell 2
	panel_default.classList.add(boogeyman);
        panel_report.classList.add(boogeyman);
        // cell 3
        panel_controls_pc.classList.add(boogeyman);
        panel_controls_ri.classList.add(boogeyman);
}
//<editor-fold desc="Event Handling" defaultstate="collapsed">
btn_back.addEventListener('click',m_crumb_nav);
_btnback.addEventListener('click',m_crumb_nav);
// cell 3
sec_preview_p1.addEventListener('click',function(e){sec_preview_nav(this);});
sec_preview_p2.addEventListener('click',function(e){sec_preview_nav(this);});
//</editor-fold >
//</editor-fold>