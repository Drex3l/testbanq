function load() {
    /* containers containing javascript content are hidden by default
     * the following lines of code removes the css class hiding them
     */
    var container = document.getElementsByClassName('js');
    for (var i = container.length - 1; i >= 0; i--) {
        container[i].classList.remove('js');
    }
    if (url_exists(window.localStorage.profile)) {	// checke if file exists
        post_ajax(window.localStorage.profile, 'action=groups', function (data) {  // load user data from database
            group_details(data); // display information to form
        });
    } else {
        input_error.classList.remove('display-none');	// invoke the error description element
        input_error.innerHTML = 'file <b>\'' + window.localStorage.profile + '\'</b> missing!';
    }
    // initializing communication panes
    let cmc = document.getElementsByClassName('cmc');
    for (let i = 0; i < cmc.length; i++) {
        cmc[i].innerHTML = cmc[i].title;
    }
    current_res();
}
document.body.onload = load;
document.body.addEventListener('keypress', function (e) {
    switch (e.key) {
        case 'F1':
            console.log('mayday!');
            break;
        case 'F4':
        case 'F8':
        case 'F9':
            test(e.key);
            break;
        default:
            // console.log(e.key);
            break;
    }
});

function group_details(data) {
    let groups = JSON.parse(data);  // json string from database
    if (groups.length < 1) {
        let cell = document.createElement('td');
        cell.classList = 'align-center';
        cell.innerHTML = 'you aren\'t part of any group yet';

        let row = document.createElement('tr');
        row.appendChild(cell);
        tbl_group_ls.appendChild(row);
    } else if(typeof groups === 'object' && !Array.isArray(groups)){
        build_group_ls(groups);
    } else {
        for (let i = 0; i < groups.length; i++) build_group_ls(groups[i]);
    }
}
function build_group_ls(groups) {
    
    let cell = [document.createElement('td'), document.createElement('td'), document.createElement('td')];
    cell[0].innerHTML = groups['GROUP_NAME'];
    if (typeof groups['DESCRIPTION'] === 'string' && groups['DESCRIPTION'].length > 0) cell[0].title = groups['DESCRIPTION'];
    cell[0].addEventListener('click', function (e) {panel_group(groups, groups['ADMIN']);});
    cell[0].addEventListener('dblclick', function (e) {
        get_ajax(window.localStorage.profile + '?action=group-select&gid=' + groups['GROUP_ID'], function (data) {
            focus_group_id.value = groups['GROUP_ID'];
            if (groups['ADMIN'] === '1')  admin_flag.classList.remove('display-none');
            else admin_flag.classList.add('display-none');
            //  accumulate breadcrumb with current panel
            let l0_link = document.createElement('a');
            l0_link.innerHTML = groups['GROUP_NAME'];

            let slice = document.createElement('li');
            slice.classList = 'crumb+';
            slice.id = 'l0_slice';
            slice.appendChild(l0_link);

            nav_crumb.appendChild(slice);
            group_select(JSON.parse(data));
        });
    });
    let btn_del = [document.createElement('i'),document.createElement('i')];
    btn_del[0].title = 'leave group';
    btn_del[0].classList = 'fa fa-remove';
    btn_del[0].name = groups['USER_ID'];
    btn_del[0].addEventListener('click', function (e) {leave_group(1,groups['GROUP_ID']);});
    
    btn_del[0].id = 'btn_rm_'+groups['GROUP_ID'];
    btn_del[1].id = 'vss_rm_'+groups['GROUP_ID'];
    btn_del[1].classList = 'fa fa-spinner fa-pulse display-none';

    cell[1].classList = 'note';
    if (groups['ADMIN'] === '1') cell[1].innerHTML = 'Admin';

    cell[2].classList = 'control';
    cell[2].title = groups['GROUP_ID'];
    for(let i = 0 ; i < btn_del.length ; i++) cell[2].appendChild(btn_del[i]);

    let row = document.createElement('tr');
    row.id = 'row_' + groups['GROUP_ID'];
    for (let k = 0; k < cell.length; k++) row.appendChild(cell[k]);

    tbl_group_ls.appendChild(row);
}
function leave_group(stage,groupid,state) {
    switch(stage){
        case 1:
            input_error.innerHTML = null;
            switch_panels(document.getElementById('vss_rm_'+groupid),document.getElementById('btn_rm_'+groupid));
            get_ajax(localStorage.profile + '?action=group-leave&gid='+groupid,function(data){leave_group(2,groupid,data);});
            break;
        case 2:
            if (state === 'gone' || state === 'done')
            {
                tbl_group_ls.removeChild(document.getElementById('row_'+groupid));
                window.location = '?action=profile-edit_grp';
                break;
            }
            input_error.classList.remove('display-none');
            input_error.innerHTML = state;
            switch_panels(document.getElementById('btn_rm_'+groupid),document.getElementById('vss_rm_'+groupid));
            break;
    }
}

function panel_group(groupinfo, admin) {
    panel_icon_ctrl.classList.add('display-none');
    btn_rm_icon.onclick = null;
    if (admin === '1') {
        window.sessionStorage.group_id = group_id.value = groupinfo['GROUP_ID'];
        txt_input_0.value = groupinfo['GROUP_NAME'];
        radio_select('group-access', groupinfo['PUBLIC']);
        ta_input_1.value = groupinfo['DESCRIPTION'];
        group_script_action.value = 'group-update';
        if(typeof groupinfo['GROUP_BANNER'] === 'string' && groupinfo['GROUP_BANNER'].length > 0) {
            panel_icon_ctrl.classList.remove('display-none');
            btn_rm_icon.onclick = function(e){
                get_ajax(window.localStorage.profile+'?action=group-rm-icon&id='+groupinfo['GROUP_ID'],function(data){
                    if(data==='success') window.location = '?action=profile-edit_grp';
                    else{
                        input_error.classList.remove('display-none');
                        input_error.innerHTML = state;
                    }
                });
            };
        }
    }
    reset_img_back();
    if(typeof groupinfo['GROUP_BANNER'] === 'string' && groupinfo['GROUP_BANNER'].length > 0) {
        let img = PATH.value+'/main/usr/img/user/group/'+groupinfo['GROUP_ID']+'.'+groupinfo['GROUP_BANNER'];
        org_avatar.src = img;
    }
}
function group_select(data){
    switch_panels(group_content, group_form);
    gmemb_count.innerHTML = data['MEMBERS'];
    grepo_count.innerHTML = data['REPOS'];
    latest_member_time.innerHTML = data['MEMB_TIME'].toString();
    latest_repo_time.innerHTML = data['REPO_TIME'].toString();
    latest_member_name.innerHTML = data['MEMB_NAME'];
    latest_repo_name.innerHTML = data['REPO_NAME'];
    
    window.sessionStorage.group_admin = data['ADMIN'];

    gmemb_tr.onclick = function (e) {
        get_ajax(window.localStorage.profile +'?action=group-members&gid=' + data['ID'],function(member){
            group_members(member,data['ADMIN']);
        });
    };
    grepo_tr.onclick = function (e) {
        get_ajax(window.localStorage.profile + '?action=group-repos&gid=' + data['ID'],function(repos){
            group_repos(repos,data['ADMIN']);
        });
    };
}
function group_members(data,admin) {
    //  accumulate breadcrumb with current panel
    let l1_link = document.createElement('a');
    l1_link.innerHTML = 'members';

    let slice = document.createElement('li');
    slice.classList = 'crumb+';
    slice.id = 'l1_slice';
    slice.appendChild(l1_link);

    nav_crumb.appendChild(slice);
    switch_panels(member_ls, group_default);
    
    //  ----------------------------------------------------------------------  new members can only be added by admin
    if (admin === '1') li_user_add.classList.remove('display-none');
    else li_user_add.classList.add('display-none');
    
    tbl_group_mbers.innerHTML = null;   //  ----------------------------------  reset list
    let members = JSON.parse(data);
    
    if (members.length < 1) {
        let cell = document.createElement('td');
        cell.classList = 'align-center';
        cell.innerHTML = 'this group has no members';

        let row = document.createElement('tr');
        row.classList = 'tr-no-member';
        row.appendChild(cell);
        tbl_group_mbers.appendChild(row);
    }
    else if(!Array.isArray(members)){
        build_member_ls(members,admin);
    }else{
        for(let i= 0;i < members.length; i++) build_member_ls(members[i],admin);
    }
}
function build_member_ls(member,admin){
    let cell = [document.createElement('td'), document.createElement('td'), document.createElement('td')];
    let full_name = (member['FIRST_NAME'] + ' ' + member['LAST_NAME']).replace('null','').replace('null','');

    cell[0].innerHTML = member['USERNAME'];
    if (full_name.trim().length > 0) cell[0].title = full_name;
    
    let lbl_admin = [document.createElement('label'),document.createElement('label')];
    
    lbl_admin[1].classList = 'fa fa-spinner fa-pulse display-none';
    lbl_admin[0].id = 'lbl_mv_'+member['USER_ID'];
    lbl_admin[1].id = 'vss_mv_'+member['USER_ID'];
    for(let i = 0;i < lbl_admin.length;i++) cell[1].appendChild(lbl_admin[i]);
    
    cell[1].classList = 'note';
    cell[1].id = 'td_' + member['USER_ID'];
    if (member['ADMIN'] === '1') lbl_admin[0].innerHTML = 'Admin';
    if(admin === '1') cell[1].addEventListener('dblclick', function (e) {alter_role(1,member['GROUP_ID'], member['USER_ID']);});
    
    let btn_del = [document.createElement('i'),document.createElement('i')];
    btn_del[0].title = 'remove member';
    btn_del[0].classList = 'fa fa-remove';
    btn_del[0].id = 'btn_rm_'+member['USER_ID'];
    btn_del[1].id = 'vss_rm_'+member['USER_ID'];
    btn_del[1].classList = 'fa fa-spinner fa-pulse display-none';
    btn_del[0].name = member['USER_ID'];
    btn_del[0].addEventListener('click', function (e) { rm_member(1,member['GROUP_ID'], member['USER_ID']);});
    
    cell[2].classList = 'control';
    if(member['USER_ID'] !== member['CLIENT']) for(let i=0;i<btn_del.length;i++) cell[2].appendChild(btn_del[i]);
    
    let cols = cell.length;
    if(admin === '0') cols--;   //  ------------------------------------------- only admin can remove members
    let row = document.createElement('tr');
    row.id = 'row_' + member['USER_ID']; 
    for (let k = 0; k < cols; k++) row.appendChild(cell[k]);
    tbl_group_mbers.appendChild(row);
}
function add_member(stage, groupid, state) {
    switch (stage) {
        case 1:
            if (txt_user_add_4.value.trim().length < 1) {
                err_field(txt_user_add_4, 'please provide username');
                switch_panels(btn_add_member, add_member_vss);
                break;
            }
            post_ajax(localStorage.profile,'action=group-add-member&uname='+txt_user_add_4.value+'&gid='+groupid, function (state) {
                add_member(2, groupid, state);
            });
            break;
        case 2:
            if (!state.includes(':'))
            {
                err_field(txt_user_add_4, state);
                switch_panels(btn_add_member, add_member_vss);	// remove 'loading' icon and return 'sign up' button
                break;
            }
            if(document.getElementsByClassName('tr-no-member').length > 0)    tbl_group_mbers.removeChild(document.getElementsByClassName('tr-no-member')[0]);
            build_member_ls(JSON.parse(state),'1');
            switch_panels(btn_add_member, add_member_vss);	// remove 'loading' icon and return 'sign up' button
            break;
    }
}
function rm_member(stage,groupid, memberid,state) {
    panel_msg(group_content_msg);   // clear message area
    switch(stage){
        case 1:
            switch_panels(document.getElementById('vss_rm_'+memberid), document.getElementById('btn_rm_'+memberid));  // replace 'delete' with spinning icon, to demonstrate progress
            post_ajax(localStorage.profile,'action=group-rm-member&uid='+memberid+'&gid='+groupid, function (state) {rm_member(2, groupid, memberid, state);});
            break
        case 2:
            if (state === 'gone')
            {
                tbl_group_mbers.removeChild(document.getElementById('row_'+memberid));
                break;
            }
            if(state === 'done') window.location = '.';
            panel_msg(group_content_msg,state,true);
            switch_panels(document.getElementById('btn_rm_'+memberid),document.getElementById('vss_rm_'+memberid));
            break;
    }
}
function alter_role(stage,groupid, memberid, state) {
    panel_msg(group_content_msg);
    switch(stage){
        case 1:
            switch_panels(document.getElementById('vss_mv_'+memberid), document.getElementById('lbl_mv_'+memberid));
            post_ajax(localStorage.profile,'action=group-mv-member&uid='+memberid+'&gid='+groupid, function (state) {alter_role(2, groupid, memberid, state);});
            break
        case 2:
            if (state === '1')
            {
                document.getElementById('lbl_mv_' + memberid).innerHTML = 'Admin';
            }else if(state === '0'){
                document.getElementById('lbl_mv_' + memberid).innerHTML = null;
                admin_flag.classList.add('display-none');
            }else{
                panel_msg(group_content_msg,state,true);
            }
            switch_panels(document.getElementById('lbl_mv_'+memberid),document.getElementById('vss_mv_'+memberid));
            break;
    }
}

function group_repos(data,admin){
    //  accumulate breadcrumb with current panel
    let l1_link = document.createElement('a');
    l1_link.innerHTML = 'repos';

    let slice = document.createElement('li');
    slice.classList = 'crumb+';
    slice.id = 'l1_slice';
    slice.appendChild(l1_link);

    nav_crumb.appendChild(slice);
    
    switch_panels(repo_ls, group_default);
    //  repo list combobox
    input_repo_select_5.innerHTML = tbl_group_repos.innerHTML = null;   //  ----------------------------------  reset list
    let repos = JSON.parse(data);
    get_ajax(window.localStorage.profile + '?action=repo-select&gid=' + focus_group_id.value,function(repo_ls){
        let item = JSON.parse(repo_ls);
        for(let i=0;i<item.length;i++) repo_combobox(item[i]['REPO_ID'],item[i]['REPO_NAME']);  // build up repo select combox with options of user repos
    });
    
    if (repos.length < 1) {
        let cell = document.createElement('td');
        cell.classList = 'align-center';
        cell.innerHTML = 'this group has no repos';

        let row = document.createElement('tr');
        row.classList = 'tr-no-repo';
        row.appendChild(cell);
        tbl_group_repos.appendChild(row);
    }
    else if(!Array.isArray(repos)){
        build_repo_ls(repos,admin);
    }else{
        for(let i= 0;i < repos.length; i++) build_repo_ls(repos[i],admin);
    }
}
function build_repo_ls(repo,admin){
    let cell = [document.createElement('td'), document.createElement('td')];

    cell[0].innerHTML = repo['REPO_NAME'];
    if (repo['USER_ID'] === repo['CLIENT']) cell[0].title = 'yours';
    else {
        let owner = repo['USERNAME'];
        if(owner[owner.length-1] === 's')  owner += '\'';
        else owner += '\'s';
        cell[0].title = owner;
    }
    
    let btn_del = [document.createElement('i'),document.createElement('i')];
    btn_del[0].title = 'remove repo';
    btn_del[0].classList = 'fa fa-remove';
    btn_del[0].id = 'btn_rm_'+repo['REPO_ID'];
    btn_del[1].id = 'vss_rm_'+repo['REPO_ID'];
    btn_del[1].classList = 'fa fa-spinner fa-pulse display-none';
    btn_del[0].name = repo['REPO_ID'];
    btn_del[0].addEventListener('click', function (e) { rm_repo(1,focus_group_id.value, repo['REPO_ID']);});
    
    cell[1].classList = 'control';
    if(repo['USER_ID'] === repo['CLIENT'] || admin === '1') for(let i=0;i<btn_del.length;i++) cell[1].appendChild(btn_del[i]);
    
    
    let cols = cell.length;
    let row = document.createElement('tr');
    row.id = 'row_' + repo['REPO_ID']; 
    for (let k = 0; k < cols; k++) row.appendChild(cell[k]);
    tbl_group_repos.appendChild(row);
}
function add_repo(stage, groupid, state){
    switch (stage) {
        case 1:
            if (input_repo_select_5.value.trim().length < 1) {
                err_field(input_repo_select_5, 'no repo selected');
                switch_panels(btn_add_repo, add_repo_vss);
                break;
            }
            post_ajax(localStorage.profile,'action=group-add-repo&rid='+input_repo_select_5.value+'&gid='+groupid, function (state) {
                add_repo(2, groupid, state);
            });
            break;
        case 2:
            if (!state.includes(':'))
            {
                err_field(input_repo_select_5, state);
                switch_panels(btn_add_repo, add_repo_vss);	// remove 'loading' icon and return 'sign up' button
                break;
            }
            //remove row reporting that there are no repos for group, if it exists
            if(document.getElementsByClassName('tr-no-repo').length > 0)    tbl_group_repos.removeChild(document.getElementsByClassName('tr-no-repo')[0]);
            // remove selected option from combobox
            input_repo_select_5.removeChild(document.getElementById('opt_'+input_repo_select_5.value));
            build_repo_ls(JSON.parse(state),window.sessionStorage.group_admin);
            switch_panels(btn_add_repo, add_repo_vss);	// remove 'loading' icon and return 'sign up' button
            break;
    }
}
function rm_repo(stage,groupid, repo_id,state){
    panel_msg(group_content_msg);   // clear message area
    switch(stage){
        case 1:
            switch_panels(document.getElementById('vss_rm_'+repo_id), document.getElementById('btn_rm_'+repo_id));  // replace 'delete' with spinning icon, to demonstrate progress
            post_ajax(localStorage.profile,'action=group-rm-repo&rid='+repo_id+'&gid='+groupid, function (state) {rm_repo(2, groupid, repo_id, state);});
            break;
        case 2:
            let token = state.split('|');
            if (token[1] === 'gone')
            {
                tbl_group_repos.removeChild(document.getElementById('row_'+repo_id));
                repo_combobox(repo_id,token[0]);
                break;
            }
            if(token[1] === 'empty'){  // if group contains no repo, return old message
                tbl_group_repos.removeChild(document.getElementById('row_'+repo_id));
                
                let cell = document.createElement('td');
                cell.classList = 'align-center';
                cell.innerHTML = 'this group has no repos';

                let row = document.createElement('tr');
                row.classList = 'tr-no-repo';
                row.appendChild(cell);
                tbl_group_repos.appendChild(row);
                repo_combobox(repo_id,token[0]);
                break;
            }
            panel_msg(group_content_msg,state,true);
            switch_panels(document.getElementById('btn_rm_'+repo_id),document.getElementById('vss_rm_'+repo_id));
            break;
    }
}
function err_field(field, txt) {
    var cmc = document.getElementById("cmc_" + field.id.substr(field.id.length - 1, 1));	//	create cmc element to display error message
    //	reseting elements
    cmc.classList.remove("err");
    cmc.innerHTML = cmc.title;
    field.classList.remove('err');
    sbm_btn.disabled = false;

    if (typeof txt === "string" && txt.length > 0) {
        cmc.classList.add("err");
        field.classList.add('err');
        sbm_btn.disabled = true;
        cmc.innerHTML = txt;	// displaying error message
    }
}
function panel_msg(field,txt,caution) {
    //	reseting elements
    field.classList.remove('err-txt');
    field.innerHTML = ':';

    if (typeof txt === "string" && txt.length > 0) {
        field.innerHTML = txt;	// displaying error message
    }
    if(caution){
        field.classList.add('err-txt');
    }
}
function repo_combobox(id, name) {
    let option = document.createElement('option');
    option.innerHTML = name;
    option.value = id;
    option.id = 'opt_' + id;
    input_repo_select_5.appendChild(option);
}
//<editor-fold desc="VALIDATION" defaultstate="collapsed">
function validate_field_0(target) {
    //	confirm input text
    if (target.value.trim().length < 1) {
        err_field(target, 'field empty');
        return false;
    }
    var txt = is_name_plus(target.value, 2, 20);	// verify correct naming conversion
    if (txt !== "success") {
        err_field(target, txt);
        return false;
    }
    if (is_symbol(target.value)) {
        err_field(target, 'symbol detected');	// check for non-alphabet symbols
        return false;
    }
    return true;
}

function save_data() {
    switch_panels(update_vss, sbm_btn);	//	hide 'sign up' button, and show 'loading' icon
    var valid = true;
    if (!validate_field_0(txt_input_0))
        valid = false;

    if (valid) {
        post_ajax(window.localStorage.profile, 'act')
        update_success.classList.remove('display-none');
        sbm_btn.innerHTML = 'saved!';
        sbm_btn.disabled = true;
    }
    switch_panels(sbm_btn, update_vss);
}
//</editor-fold>
//<editor-fold desc="event handling" defaultstate="collapsed">
txt_input_0.addEventListener('input', function (e) {
    err_field(this);
});
txt_input_0.addEventListener('change', function (e) {
    validate_field_0(this);
});

ta_input_1.addEventListener('input', function (e) {
    err_field(this);
});

txt_user_add_4.addEventListener('input', function (e) {
    err_field(this);
});
sbm_btn.addEventListener('click', save_data);
clr_btn.addEventListener('click',function(e){
    group_icon_base64.value = file_input_2.value = null;
    group_id.value = null;
    group_script_action.value = 'group-add';
    reset_img_back();
});
btn_add_member.addEventListener('click', function (e) {
    switch_panels(add_member_vss, this);
    panel_msg(group_content_msg);
    add_member(1, group_id.value);
});
btn_add_repo.addEventListener('click',function(e){
    switch_panels(add_repo_vss, this);
    panel_msg(group_content_msg);
    add_repo(1, focus_group_id.value);
});
btnback_form.addEventListener('click', function (e) {
    let crumb = document.getElementsByClassName('crumb+');
    switch(crumb.length){
        case 1:
            switch_panels(group_form, group_content);
            focus_group_id.value = null;
            break;
        case 2:
            member_ls.classList.add('display-none');
            repo_ls.classList.add('display-none');
            group_default.classList.remove('display-none');
            get_ajax(window.localStorage.profile + '?action=group-select&gid=' + window.sessionStorage.group_id, function (data) {group_select(JSON.parse(data));});
            break;
    }
    
    panel_msg(group_content_msg);
    nav_crumb.removeChild(nav_crumb.lastChild);
});

//<editor-fold desc="img crop" defaultstate="collapsed">
function group_icon_crop() {
    c.result('canvas').then(function (canvas) {
        org_avatar.src = group_icon_base64.value = canvas;
        switch_panels(modal_btn_crop,img_vss);
        modal_img.classList.add('display-none');
    }).catch(function (ex) {
        window.alert(ex.message);
    });
}
file_input_2.addEventListener('change', function (e) {
    modal_img.classList.remove('display-none');
    modal_btn_crop.onclick = group_icon_crop;
    c = upload_img(this, modal_crop_space, null, 320, 320, false, 'img-rotate', 'Group Icon', fetch_canvas);
    modal_btn_crop.name = 'org_avatar';
});
btn_rm_icon.addEventListener('click', function (e) {
    panel_icon_ctrl.classList.add('display-none');
    let parent = org_avatar.parentNode;
    parent.removeChild(org_avatar);
    let img = document.createElement('img');
    img.id = 'org_avatar';
    parent.appendChild(img);
    file_input_2.value = null;
});
//</editor-fold>
//</editor-fold>
