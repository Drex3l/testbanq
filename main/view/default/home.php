<!DOCTYPE html>
<html lang="en">
    <?php require_once "head.php"; ?>
    <body id="home_page">
        <section id="page_wrap" class="page-wrap main flash">
            <header class="main flash">
                <div class="container">
                    <div id="branding" class="float-left">
                        <a href="." title="home">
                            <img src="<?= PATH; ?>/main/usr/img/user/dev/logo-sanhedrin.png" alt="Logo" class="c2r" />
                        </a>
                    </div>
                    <?php require_once "header-$session.php"; ?>
                </div>
            </header>
            <?php require_once dirname(__FILE__, 3) . '/epiqworx/template/noscript.html'; ?>
            <section  id="showcase" class="">
                <h1 class="motto">The Lecturer Best Friend</h1>
            </section>
            <?php require_once dirname(__FILE__, 3) . '/epiqworx/template/modal-glass.html'; 
            require_once "content-$session.php";?>
        </section>
        <?php require_once 'footer.php'; ?>
        
    </body>
</html>