<!DOCTYPE html>
<html lang="en">
    <?php require_once  dirname(__FILE__, 2) .  "/default/head.php"; ?>
    <body id="db_error_page">
        <section id="page_wrap" class="page-wrap main flash">
            <header class="main flash">
                <div class="container">
                    <div id="branding" class="float-left">
                        <a href="." title="home">
                            <img src="<?= PATH; ?>/main/usr/img/user/dev/logo-sanhedrin.png" alt="Logo" />
                        </a>
                    </div>
                </div>
            </header>
            <section class="page-wrap error-page">
                <div>
                    <h1>Database Error</h1>

                <?php
                switch ($case) {
                    case 'connect':
                        ?>
                        <p>There was an error establishing a connection</p>
                        <table>
                            <tr><td>Code</td><td style="font-weight: 700">&nbsp;<?php echo substr($error_message, 0, 22); ?></td></tr>
                            <tr><td>Description</td><td>&nbsp;&nbsp;<?php echo substr($error_message, 23, (strlen($error_message)) - 23); ?>&nbsp;&nbsp;</td></tr>
                        </table>
                        <?php
                        global $cs_file;
                        global  $cs_path;
                        if (file_exists(dirname(__FILE__, 4) . "/".self::$cs_path."/".self::$cs_file)) { echo '<p class="info-plus" style="text-align:center">please check your <b>'.self::$cs_file.'</b> file</p>'; }
                        break;
                    case 'DML':
                        $piece = explode(':', $error_message);
                        ?>
                        <p>Execution Error @<?= $piece[0]; ?></p>
                        <p>Error Code : <?= $piece[1]; ?></p>
                        <p>Description : <?= $piece[2]; ?></p>
                    <?php break;
                default:
                    ?>
                        <p>Generic Error</p>
                        <table>
                            <tr><td>Code</td><td style="font-weight: 700">&nbsp;<?php echo substr($error_message, 0, 15); ?></td></tr>
                            <tr><td>Description</td><td>&nbsp;&nbsp;<?php echo substr($error_message, 17, (strlen($error_message))); ?>&nbsp;&nbsp;</td></tr>
                        </table>
                    <?php
                    break;
            }
            ?>
                        </div>
            </section>
        </section>
        <?php require_once dirname(__FILE__, 2) . '/default/footer.php'; ?>
    </body>
</html>
