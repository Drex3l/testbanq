<!DOCTYPE html>
<html lang="en">
    <?php require_once dirname(__FILE__, 2) .  "/default/head.php"; ?>
    <body id="error_page">
        <section id="page_wrap" class="page-wrap main flash">
            <header class="main flash">
                <div class="container">
                    <div id="branding" class="float-left">
                        <a href="." title="home">
                            <img src="<?= PATH; ?>/main/usr/img/user/dev/logo-sanhedrin.png" alt="Logo" />
                        </a>
                    </div>
                </div>
            </header>
            <section class="page-wrap error-page">
              <style>
              </style>
                <div>
                    <?php if (isset($error_message)) { ?><p><?= $error_message; ?></p><?php } ?>
                    <table cellpadding="2" style="margin: 0 auto;text-align: left;border: 1px dashed #efefef">
                        <?php
                        if (isset($error)) {
                            for ($k = 0; $k < count($error); $k++) {
                                ?>
                        <tr><td>&nbsp;&nbsp;E<?= str_pad(($k+1), 2, '0', STR_PAD_LEFT); ?>&nbsp;&nbsp;</td><td style="font-weight: 700">&nbsp;&nbsp;<?= $error[$k]; ?>&nbsp;&nbsp;</td></tr>
                            <?php
                            }
                        }
                        ?>
                    </table><br/>
                    <p style="color: #000;text-align: center">we apologize for this <b style="background-color: #444;color: #d5d5d5;height: 100%">&nbsp;&nbsp;inconvenience&nbsp;&nbsp;</b></p>
                </div>
            </section>
        </section>
        <?php require_once dirname(__FILE__, 2) . '/default/footer.php'; ?>
    </body>
</html>
