# testBanq
![BANNER](https://sict-iis.nmmu.ac.za/testbank/main/usr/img/sys/qb-ink.png)
``` 
the lecturer's best friend
```
## Description

The **testbanq** is a web interface to a data bank of questions used to access students in an academic institution. 
This project is opensource, powered by the **LAMP** statck, and versioned by **Git**.

Project core implements the **MVC** design pattern.

## Installation
Clone this repository to your document root, and with web services running on your localhost, try accessing website from your web browser with the loopback address.
```
NOTE: Some systems might require user to append colon, followed by port number, to loopback address, when accessing a website in Document Root.
```
### Requirements
* Apache2 (web server)
* MariaDB/MySQL 5.7 (database server)
* _php_ 7.2 (server-side scripting)
* Git (version control system)

### File System
Your web server needs to be given privileges to allow full control of the testbanq and it's sub directories.
Originaly, the root of the testbanq directory should contain the following files (in **bold** are directories)
* **main/**
* **profile/**
* **work/**
* **.git/**
* index.php
* README.md
* .gitignore

Accessing website from web browser should created the 'connection.ini' file, inside the **main/config/** path. Use this file to give values of the connection string.
```ini
[SQL]
host = <database host ip address>
user = <database username>
password = <database user password>
dbname = <database schema>
```
### Database
The subdirectory **main/config/** also contails files that set up the testbanq database using files found in the **main/usr/sql/** path.

These files assume that the path to MariaDB/MySQL **bin** directory has been added to the **Environment Variable**.
If not, please see manual of your system on how to do so, before executing these files.

#### GNU/Linux systems
The **db-gnu.sh** shell script must be executed from the **main/config/** working directory, for it use **relative paths** from its directory to reference files in the **main/usr/sql/** path. Executing this shell script from a different working directory will result in a "FILE NOT FOUND" exception.
This script is designed to work with bash , and related shells.

#### Windows system
Executing the **db-win32.bat** batch file opens a terminal window to perform the same function as the **db-gnu.sh** shell script, but on Windows systems.
The terminal window then disappears when processes are completed.

#### SQL files
The subdirectory **main/usr/sql/** contails the following **.sql** files.
* db-admin.sql (creates database user and grants SELECT to 'world' database)
* db-schema.sql	(creates relevant schema objects)
* db-data.sql	(provides required data)

### SMTP
The testbank uses the *_'phpmailer'_* library to handle outgoing mails.  
To use library however, essentially 5 arguments must be provided, by means of the **main/config/connection.ini** file.  
The following code segment illustrates the 5 arguments to be catered for to use SMTP.
```ini
[mail function]
;;;;;;;;;;;;;;;;
; Account Info ;
;;;;;;;;;;;;;;;;
smpt_host = <account smtp host address>
smpt_secure = <security mode>
smtp_port   = <port number>
email_acc = <account email address>
email_pass = <email account password>
```
Append these details to the **main/config/connection.ini** file.