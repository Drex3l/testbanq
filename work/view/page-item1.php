<!DOCTYPE html>
<html lang="en">
    <?php require_once dirname(__FILE__, 3) . "/main/view/default/head.php"; ?>
    <body id="workspace1" class="workspace">
        <section id="page_wrap" class="page-wrap main">
            <header class="main">
                <div class="container">
                    <div id="branding" class="float-left">
                        <a href="." title="home"><img src="<?= $ICON; ?>" alt="Logo" /></a>
                    </div>
                    <?php require_once dirname(__FILE__, 3) . "/main/view/default/header-$session.php"; ?>
                </div>
            </header>
            <?php require_once 'nav.php'; ?>
            <div class="main table">
                <div id="list_panel" class="table-cell  list-panel">
                    <div id="form_add_item" class="display-none">
                        <input type="hidden" name="action" value="create">
                        <input type="hidden" name="target" value="this"/>
                        <ul class="streight-nav">
                            <li class="popdown">
                                <input maxlength="20" id="txt_instance_name" name="txt-instance-name" required type="text">
                                <span id="popup_txt_instance_name" class="text error"></span>
                            </li>
                            <li>
                                <button id="btn_add_instance" class="button_1">save</button>
                                <button id="instance_add_vss" class="display-none vss button_1"><i  class="fa fa-spinner fa-pulse"></i></button>
                            </li>
                        </ul>
                    </div>
                    <div id="instance_ls_shield" class="display-none"></div>
                    <button class="collapsible" id="btn_local_repo">local<span id="repo_count_local" title="repo count" class="instance-count"></span></button>
                    <div class="fold">
                        <ul id="instance_list"></ul>
                    </div>
                </div>
                <div id="panel_content" class="table-cell x768 content">
                    <div id="panel_default" class="text-pane">
                        <div id="instance_info">
                            <h1 id="instance_h1">No Repository Selected</h1>
                            <p id="instance_p1">Please select Repository to your left.</p>
                            <p id="instance_p2">Or use large cross to add one.</p>
                            <p id="instance_err"></p>
                        </div>
                        <div id="ls_quest" class="ls display-none">
                            <div class="ls-wrap">
                                <table id="tbl_ls_x1" class="dull" quest-preview="0"></table>
                                <img id="fig_preview" class="display-none" alt="FIGURE" />
                                <i id="ls_quest_vss"  class="fa fa-spinner fa-pulse display-none"></i>
                            </div>
                        </div>
                    </div>
                    <div id="panel_dml" class="input display-none">
                        <div class="input-form">
                            <input type="hidden" name="action" value="update">
                            <input type="hidden" name="target" value="this"/>
                            <input type="hidden" name="id" id="instance_id" value="">
                            <ul class="form-input">
                                <li>
                                    <label class="prompt float-left">Repository Name</label>
                                    <label class="required float-right">*</label>
                                    <input required id="tf_instance_name_00" name="name" maxlength="20" class="instance-input" type="text"/>
                                    <span class="cmc" id="cmc_00" title="means to identify repository"></span>
                                </li>
                                <li>
                                    <label class="prompt float-left">Description</label>
                                    <textarea name="desc" id="ta_instance_desc_01" class="instance-input"></textarea>
                                    <span class="cmc" id="cmc_01" title="substantial info on the repository"></span>
                                </li>
                                <li>
                                    <label class="prompt float-left">Icon</label>
                                    <input class="instance-input" id="input_instance_02" accept="image/*" type="file"/>
                                    <input type="button" value="view current icon" id="m_btn_view_icon" class="small display-none"/>
                                    <span class="cmc" id="cmc_02" title="image associated with repository"></span>
                                </li>
                                <li>
                                    <hr/>
                                    <span id="input_error_instance" class="message  err-msg"></span>
                                    <button id="btn_reset_instance" class="button_1" title="reset data" type="reset">clear</button>
                                    <button id="btn_update_instance" class="button_1" title="update data" type="button">save</button>
                                    <button id="instance_update_vss" class="display-none vss button_1"><i  class="fa fa-spinner fa-pulse"></i></button>
                                    <div class="instance_success-tick display-none" id="update_success">✔</div>
                                </li>
                            </ul>
                        </div>
                    </div>
                    <div id="panel_index1_dml" class="display-none">
                        <div id="panel_x101" class="panel_instance_section">
                            <span class="input-form">
                                <ul class="form-input">
                                    <li>
                                        <label class="prompt float-left">Question Type</label>
                                        <label class="required float-right">*</label>
                                        <select  name="item-type" id="input_quest_07"></select>
                                        <span id="cmc_07" class="cmc" title="categorize question"></span>
                                    </li>
                                    <li>
                                        <hr class="big"/>
                                        <button id="vss_add_x1item" class="display-none vss button_1"><i  class="fa fa-spinner fa-pulse"></i></button>
                                        <button id="btn_add_x1item" type="button" title="create question" class="button_1">save</button>
                                        <span id="input_error_qtype" class="message  err-msg"></span>
                                    </li>
                                </ul>
                            </span>
                        </div>
                        <div id="panel_x102" class="panel_instance_section display-none">
                            <span class="quest-title qtitle-dynamic"></span>
                            <span class="input-form">
                                <ul class="form-input">
                                    <li>
                                        <label class="prompt float-left">Language</label>
                                        <label class="required float-right">*</label>
                                        <select name="item-class" id="input_quest_09"></select>
                                        <span id="cmc_09" class="cmc" title="select question text language"></span>
                                    </li>
                                    <li>
                                        <label class="prompt float-left">Text</label>
                                        <label class="required float-right">*</label>
                                        <textarea name="x1sec02-text" id="input_quest_10" required></textarea>
                                        <span id="cmc_10" class="cmc" title="actual question text"></span>
                                    </li>
                                    <li>
                                        <hr class="big"/>
                                        <button id="vss_save_x1sec02" class="display-none vss button_1"><i  class="fa fa-spinner fa-pulse"></i></button>
                                        <button id="btn_save_x1sec02" type="button" title="add content" class="button_1">save</button>
                                        <button id="btn_next_x1sec02" type="button" title="proceed" class="button_1 next display-none">next</button>
                                        <br/><span id="input_error_qtext" class="message err-msg"></span>
                                    </li>
                                </ul>
                            </span>
                        </div>
                        <div id="panel_x103" class="panel_instance_section display-none">
                            <span class="quest-title">column match</span>
                            <span class="input-form">
                                <ul class="form-input">
                                    <li>
                                        <label class="prompt float-left">Image</label>
                                        <label class="required float-right">*</label>
                                        <input id="input_quest_11" type="file" accept="image/*" required="" />
                                        <span id="cmc_11" class="cmc"title="must contain table with values and keywords to match"></span>
                                    </li>
                                    <li class="functionality">
                                        <label class="prompt float-left">Range</label>
                                        <label class="required float-right">*</label>
                                        <input type="number" id="q_from_12" min="1" placeholder="start from" />
                                        <input type="number" id="q_to_12" min="1" placeholder="end at" />
                                        <span id="cmc_12" class="cmc" title="select range of questions for this"></span>
                                    </li>
                                    <li>
                                        <hr class="big"/>
                                        <button id="vss_save_x1qcm" class="display-none vss button_1"><i  class="fa fa-spinner fa-pulse"></i></button>
                                        <button id="btn_save_x1qcm" type="button" title="add content" class="button_1">save</button>
                                        <button id="btn_next_x1qcm" type="button" title="proceed" class="button_1 next display-none">next</button>
                                        <br/><span id="input_error_qcm" class="message  err-msg"></span>
                                    </li>
                                </ul>
                            </span>
                        </div>
                        <div id="panel_x104" class="panel_instance_section display-none">
                            <span class="quest-title qtitle-dynamic"></span>
                            <span class="input-form">
                                <ul class="form-input">
                                    <input type="hidden" id="input_ansr_text" />

                                    <li id="sec_ansr_mc">
                                        <label class="prompt float-left">Text</label>
                                        <label class="required float-right">*</label>
                                        <input type="text" id="input_ansr_14" name="answer-text"/>
                                        <span id="cmc_14" class="cmc" title="one of the possible answer options"></span>
                                    </li>
                                    <li id="sec_ansr_tf">
                                        <label class="prompt float-left">Value</label>
                                        <label class="required float-right">*</label>
                                        <select id="input_ansr_15">
                                            <option id="opt_ansr_T" value="T">True</option>
                                            <option id="opt_ansr_F" value="F">False</option>
                                        </select>
                                        <span id="cmc_15" class="cmc" title="one of these must have a misconception"></span>
                                    </li>
                                    <li id="sec_ansr_sate">
                                        <label class="prompt float-left">Answer</label>
                                        <label class="required float-right">*</label>
                                        <label class="radio">Correct<input type="radio" required id="input_ansr1_08" value="1" name="ansr-state"/></label>
                                        <label class="radio">Incorrect<input type="radio" required id="input_ansr0_08" value="0" name="ansr-state"/></label>
                                        <span id="cmc_08" class="cmc" title="select if answer is correct or nt"></span>
                                    </li>
                                    <li id="sec_misconcept" class="">
                                        <label class="prompt float-left">Misconception</label>
                                        <textarea id="input_ansr_13" name="misconception-text"></textarea>
                                        <span id="cmc_13" class="cmc" title="the misconception for wrong8 answer"></span>
                                    </li>
                                    <li>
                                        <hr class="big"/>
                                        <button id="vss_add_x1sec04" class="display-none vss button_1"><i  class="fa fa-spinner fa-pulse"></i></button>
                                        <button id="btn_save_x1sec04" type="button" title="add content" class="button_1">save</button>
                                        <button id="btn_next_x1sec04" type="button" title="proceed" class="button_1 next display-none">next</button>
                                        <button id="btn_done_x1sec04" type="button" title="finalize" class="button_1 done display-none">done</button>
                                        <br/><span id="input_error_qansr" class="message  err-msg"></span>
                                    </li>
                                </ul>
                            </span>
                        </div>
                        <div id="panel_x105" class="panel_instance_section display-none">
                            <span class="quest-title qtitle-dynamic"></span>
                            <span class="input-form">
                                <ul class="form-input">
                                    <li>
                                        <label class="prompt float-left">Figure Type</label>
                                        <select name="figure-type" id="input_quest_16"></select>
                                        <span id="cmc_16" class="cmc" title="shortlist figure list"></span>
                                    </li>
                                    <li>
                                        <label class="prompt float-left">Figure List</label>
                                        <label class="required float-right">*</label>
                                        <select name="figure-list" id="input_quest_17"></select>
                                        <span id="cmc_17" class="cmc" title="select figure from list"></span>
                                    </li>
                                    <li class="list">
                                        <table id="tbl_quest_fig_ls" class="dull"></table>
                                    </li>
                                    <li>
                                        <hr class="big"/>
                                        <button id="vss_add_x1sec05" class="display-none vss button_1"><i  class="fa fa-spinner fa-pulse"></i></button>
                                        <button id="btn_save_x1sec05" type="button" title="update questiont" class="button_1">save</button>
                                        <button id="btn_done_x1sec05" type="button" title="finalize" class="button_1 done display-none">done</button>
                                        <br/><span id="input_error_qansr" class="message  err-msg"></span>
                                    </li>
                                </ul>
                            </span>
                        </div>
                        <footer class="menu-nav">
                            <ul id="quest_nav_bar" class="streight-nav">
                                <li id="nav_section_01" class="btn_nav_section" curr-state="na"><span>New</span><div class="btn-shield"></div></li>
                                <li id="nav_section_02" class="btn_nav_section" curr-state="na"><span>Question</span><div class="btn-shield"></div></li>
                                <li id="nav_section_03" class="btn_nav_section" curr-state="na"><span>Questions</span><div class="btn-shield"></div></li>
                                <li id="nav_section_04" class="btn_nav_section" curr-state="na"><span>Answers</span><div class="btn-shield"></div></li>
                                <li id="nav_section_05" class="btn_nav_section" curr-state="na"><span>Figures</span><div class="btn-shield"></div></li>
                            </ul>
                        </footer>
                    </div>
                    <div id="panel_index2_dml" class="input display-none">
                        <div  class="input-form">
                            <input name="action" value="create" type="hidden"/>
                            <input name="target" value="figure" type="hidden"/>
                            <input name="repo-id" id="figure_repo_id" type="hidden">
                            <ul class="form-input">
                                <li>
                                    <label class="prompt float-left">Figure Label</label>
                                    <label class="required float-right">*</label>
                                    <input id="input_figure_03" name="figure-label" maxlength="20" type="text" required="">
                                    <span class="cmc" id="cmc_03" title="assign label to figure"></span>
                                </li>
                                <li>
                                    <label class="prompt float-left">Description</label>
                                    <textarea name="figure-desc" id="input_figure_4"></textarea>
                                    <span class="cmc" id="cmc_04" title="give figure description"></span>
                                </li>
                                <li>
                                    <label class="prompt float-left">File</label>
                                    <label class="required float-right">*</label>
                                    <input id="input_figure_05" accept="image/*" type="file" name="file-name" required="">
                                    <span class="cmc" id="cmc_05" title="locate figure image file"></span>
                                </li>
                                <li>
                                    <label class="prompt float-left">Figure Type</label>
                                    <input id="input_figure_06" name="figure-type" maxlength="255" type="text" list="x2_type_ls"/>
                                    <datalist id="x2_type_ls"></datalist>
                                    <span class="cmc" id="cmc_06" title="chart, graph, graphic, model, table, etc..."></span>
                                </li>
                                <li>
                                    <hr class="big">
                                    <span id="input_error_figure" class="message"></span>
                                    <button id="btn_reset_x2item_data" type="reset" title="reset data" class="button_1">clear</button>
                                    <button id="btn_save_figure" type="button" title="add Figure" class="button_1 position-relative">save<span class="success-tick display-none" id="x2_update_success">✔</span></button>
                                    <button id="x2_update_vss" class="display-none vss button_1"><i  class="fa fa-spinner fa-pulse"></i></button>
                                </li>
                            </ul>
                        </div>
                    </div>
                    <span id="content_err" class="err-report"></span>
                </div>
                <div id="panel_dash_aside" class="sidebar table-cell x768">
                    <div id="panel_quest_intro" class="display-none quest-dml">
                        <ul class="notice">
                            <li>saving question pushes it directly to database</li>
                            <li>upon save, tabs to complete process are activated</li>
                            <li>subsequent changes update question in real-time</li>
                        </ul>
                        <img src="<?= PATH; ?>/main/usr/img/user/dev/compact-sanhedrin.png" alt="watermark" class="watermark">
                    </div>
                    <div id="panel_quest_ls" class="display-none quest-dml">
                        <table id="tbl_quest_text_ls" class="dull special tbl-quest"></table>
                        <table id="tbl_quest_ansr_ls" class="dull special tbl-quest"></table>
                    </div>
                    <div id="panel_quest_fig" class="display-none quest-dml">
                        <img id="img_quest_fig" alt="FIGURE" />
                    </div>
                    <div id="panel_index2_ls" class="display-none">
                        <div class="ls">
                            <div class="ls-wrap">
                                <table id="tbl_ls_x2" class="dull"></table>
                            </div>
                        </div>
                    </div>
                    <div id="panel_quest_preview" class="display-none">
                        <div id="panel_quest_form" class="">
                            <table id="tbl_quest_preview" class="quest"></table>
                        </div>
                        <div id="panel_quest_tex" class=" display-none">
                            <pre id="pre_tex_out"></pre>
                        </div>
                        <footer class="menu-nav">
                            <ul id="prev_switch_bar" class="streight-nav">
                                <li id="sec_quest_form" class="quest-preview-nav active"><span>Form</span></li>
                                <li id="sec_quest_tex" class="quest-preview-nav"><span>LaTeX</span></li>
                            </ul>
                        </footer>
                    </div>
                    <div id="panel_img" class="display-none">
                        <div id="item_icon_ctrl" class="display-none">
                            <span class="float-right">
                                <input type="hidden" name="action" value="rm"/>
                                <input type="hidden" value="this-icon" name='target'/>
                                <input type="hidden" name="id"id="rm_instance_icon" />
                                <button title="remove icon" id="btn_rm_repo_icon"><i class="fa fa-close"></i></button>
                                <button id="vss_rm_repo_icon" class="display-none"><i class="fa fa-spinner fa-pulse"></i></button>
                            </span>
                        </div>
                        <img id="instance_icon_display" class="dim" alt="banner" src="<?= PATH; ?>/main/usr/img/user/dev/compact-sanhedrin.png">
                    </div>
                    <div id="panel_crop_space" class="display-none">
                        <div id="crop_space" class="crop-space"></div>
                        <div class="controls">
                            <span class="left big this-rotate img-rotate" title="90"><i class="fa fa-rotate-left"></i></span>
                            <span class="right big this-rotate img-rotate" title="-90"><i class="fa fa-rotate-right"></i></span>
                            <button id="btn_crop" title="crop" class="bottom" type="button"><i class="fa fa-crop"></i></button>
                            <button id="btn_crop_vss" title="crop" class="bottom display-none" type="button"><i  class="fa fa-spinner fa-pulse"></i></button>
                            <button id="btn_close_crop" title="cancel" class="top close_crop" type="button"><i class="fa fa-close"></i></button>
                            <button id="btn_detach_crop" title="detatch" class="big bottom" type="button"><i class="fa fa-window-restore"></i></button>
                        </div>
                    </div>
                </div>
            </div>
            <div class="footer big-1280">
                <div class="sidebar">:</div>
                <div id="panel_bottombar_ctrl" class="content">
                    <div id="wrap_btn_back" class="icon-wrap display-none">:
                        <a id="btn_back" href="javascript:" title="back one level" >
                            <span><i class="fa fa-chevron-left"></i></span>
                        </a>
                    </div>
                    <ul id="bottom_nav_crumb">
                        <li id="bn_l0"><a><?= $title; ?></a></li>
                    </ul>
                </div>
                <div class="content r">
                    <ul class="streight-nav">
                        <li><div>Questions ::&nbsp;&nbsp;<span class="value" id="osd_3">null</span><span id="link_addx1" title="add Question" class="link_addx display-none"><i class="fa fa-plus"></i></span></div></li>
                        <li><div>Figures ::&nbsp;&nbsp;<span class="value" id="osd_2">null</span><span id="link_addx2" title="add to Figure" class="link_addx display-none"><i class="fa fa-plus"></i></span></div></li>
                        <li><div>Groups ::&nbsp;&nbsp;<span class="value" id="osd_1">null</span></div></li>
                    </ul>
                </div>
            </div>
        </main>
    </article>
</div>

<div class="small-1280 m-osd">
    <i id="m_tbl_osd_toggle" class="fa fa-info" title="toggle OSD visibility"></i>
    <table id="m_tbl_osd" class="osd display-none">
        <tr class="item">
            <td rowspan="2" class="ctrl-l ctrl l"><button id="btn_addx1" class="link_addx display-none"><i class="fa fa-plus"></i></button></td>
            <td class="index">?s</td><td class="gap" rowspan="2"></td>
            <td rowspan="2" class="ctrl-l ctrl l"><button id="btn_addx2" class="link_addx display-none"><i class="fa fa-plus"></i></button></td>
            <td class="index">Figures</td><td rowspan="2" class="ctrl r"></td>
            <td class="gap" rowspan="2"></td>
            <td class="index">Groups</td>
        </tr>
        <tr class="values">
            <td id="m_osd_3">null</td><td id="m_osd_2">null</td><td id="m_osd_1">null</td>
        </tr>
    </table>
</div>
</section>
<?php
require_once dirname(__FILE__, 3) . '/main/epiqworx/template/modal-img.php';
require_once dirname(__FILE__, 3) . '/main/epiqworx/template/context-menu.html';
require_once dirname(__FILE__, 3) . "/main/view/default/footer.php";
?>
<script src="<?= PATH; ?>/main/usr/library/croppie/croppie.js"></script>
<script type="text/javascript" src="<?= PATH; ?>/main/epiqworx/logic/cropper.js"></script>
<script src="<?= PATH; ?>/main/usr/library/croppie/croppie.js"></script>
<script type="text/javascript" src="<?= PATH; ?>/main/epiqworx/logic/cropper.js"></script>
<script type="text/javascript" src="<?= PATH; ?>/main/usr/js/dash/item1.js"></script>
<script type="text/javascript" src="<?= PATH; ?>/main/usr/js/dash/media.js"></script>
</body>
</html>
