<!DOCTYPE html>
<html lang="en">
    <?php require_once dirname(__FILE__, 3) . "/main/view/default/head.php"; ?>
    <body id="workspace3" class="workspace">
        <section id="page_wrap" class="page-wrap main">
            <header class="main">
                <div class="container">
                    <div id="branding" class="float-left">
                        <a href="." title="home"><img src="<?= $ICON; ?>" alt="Logo" /></a>
                    </div>
                    <?php require_once dirname(__FILE__, 3) . "/main/view/default/header-$session.php"; ?>
                </div>
            </header>
            <?php require_once 'nav.php'; ?>
            <div class="main table">
                <div id="list_panel" class="table-cell list-panel">
                    <div id="instance_ls_shield" class="display-none"></div>
                    <ul id="instance_list"></ul>
                </div>
                <div id="panel_content" class="table-cell x768 content">
                    <div id="panel_default" class="text-pane">
                        <div id="instance_info">
                            <h1 id="instance_h1">No Script Selected</h1>
                            <p id="instance_p1">Please select Script to your left.</p>
                            <p id="instance_p2">Or use large cross to add one.</p>
                            <p id="instance_err"></p>
                        </div>
                    </div>
                    <div id="panel_dml" class="display-none">
                        <div class="input-form">
                            <ul class="form-input">
                                <li>
                                    <label class="prompt float-left">Module Code</label>
                                    <input type="text" id="input_script_00" maxlength="5" />
                                    <span class="cmc" id="cmc_00" title="this will get info about module"></span>
                                </li>
                                <li>
                                    <label class="prompt float-left">Description</label>
                                    <textarea id="input_script_01"></textarea>
                                    <span class="cmc" id="cmc_01" title="any addional info about paper e.g. paper #"></span>
                                </li>
                                <li>
                                    <label class="prompt float-left">Date + Time</label>
                                    <input type="date" id="input_script_date_02" />
                                    <input type="time" id="input_script_time_02" />
                                    <span class="cmc" id="cmc_02" title="specify when will the exam date/time"></span>
                                </li>
                                <li>
                                    <label class="prompt float-left">venue</label>
                                    <input type="text" id="input_script_03" />
                                    <span class="cmc" id="cmc_03" title="where will the paper be written?"></span>
                                </li>
                                <li>
                                    <hr/>
                                    <span id="input_error_instance" class="message  err-msg"></span>
                                    <button id="btn_save_instance" class="button_1" title="update data" type="button">save</button>
                                    <button id="vss_save_instance" class="display-none vss button_1"><i  class="fa fa-spinner fa-pulse"></i></button>
                                    <div class="instance_success-tick display-none" id="update_success">✔</div>
                                </li>
                            </ul>
                        </div>
                    </div>
                </div>
                <div id="panel_dash_aside" class="table-cell x768 sidebar">
                    
                </div>
            </div>
            <div class="footer big">
                <div class="sidebar">:</div>
                 <div id="panel_bottombar_ctrl" class="content">
                    <div id="wrap_btn_back" class="icon-wrap display-none">:
                        <a id="btn_back" href="javascript:" title="back one level" >
                            <span><i class="fa fa-chevron-left"></i></span>
                        </a>
                    </div>
                    <ul id="bottom_nav_crumb">
                        <li id="bn_l0"><a><?= $title; ?></a></li>
                    </ul>
                </div>
                <div class="content r">
                    <ul class="streight-nav">
                        <li><div>Questions ::&nbsp;&nbsp;<span class="value" id="osd_3">null</span><span id="link_addx1" title="add Question" class="link_addx display-none"><i class="fa fa-plus"></i></span></div></li>
                        <li><div>Scripts ::&nbsp;&nbsp;<span class="value" id="osd_1">null</span></div></li>
                    </ul>
                </div>
            </div>
        </article>
    </div>
    <div class="small m-osd">
        <i id="m_tbl_osd_toggle" class="fa fa-info" title="toggle OSD visibility"></i>
        <table id="m_tbl_osd" class="osd display-none">
            <tr class="item">
                <td rowspan="2" class="ctrl-l ctrl l"><button id="btn_addx1" class="link_addx display-none"><i class="fa fa-plus"></i></button></td>
                <td class="index">?s</td><td class="gap" rowspan="2"></td>
                <td class="gap" rowspan="2"></td>
                <td class="index">Scripts</td>
            </tr>
            <tr class="values">
                <td id="m_osd_3">null</td><td id="m_osd_1">null</td>
            </tr>
        </table>
    </div>
</section>
<?php require_once dirname(__FILE__, 3) . "/main/view/default/footer.php"; ?>
<script src="<?= PATH; ?>/main/usr/library/croppie/croppie.js"></script>
<script type="text/javascript" src="<?= PATH; ?>/main/epiqworx/logic/cropper.js"></script>
<script src="<?= PATH; ?>/main/usr/library/croppie/croppie.js"></script>
<script type="text/javascript" src="<?= PATH; ?>/main/epiqworx/logic/cropper.js"></script>
<script type="text/javascript" src="<?= PATH; ?>/main/usr/js/dash/item3.js"></script>
<script type="text/javascript" src="<?= PATH; ?>/main/usr/js/dash/media.js"></script>
</body>
</html>
