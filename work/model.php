<?php
abstract class Item1{
    public static function ls(){
        return dbHandler::dql('SELECT r.*, USERNAME FROM repo r, user u WHERE r.USER_ID = u.USER_ID AND r.USER_ID = :uid', array(':uid'=>$_SESSION['id']), true);
    }
    public static function ls_ready(){
        return dbHandler::dql('SELECT * FROM REPO_LIST WHERE USERNAME = :uname', array(':uname'=>$_SESSION['uname']), TRUE);
    }
    public static function add($name,$ajax=false){
        $param = array(':name'=>$name,':uid'=> $_SESSION['id']);
        return dbHandler::dql('call sp_add_repo(:name,:uid)',$param,$ajax);
    }
    public static function get_name($id){
        return dbHandler::dql('SELECT REPO_NAME FROM repo WHERE REPO_ID = :id', array(':id'=>$id), true)['REPO_NAME'];
    }

    public static function rm($id){
        return dbHandler::dql('SELECT rm_repo(:id,:client) STATE', array(':id'=>$id,':client'=>$_SESSION['id']),true)['STATE'];
    }
    public static function update($id,$name,$desc,$ajax=false){
        $param = array(':id'=>$id,':name'=>$name,':desc'=>$desc,':client'=>$_SESSION['id']);
        return dbHandler::dql('SELECT mv_repo(:name,:desc,:id,:client) STATE',$param,$ajax)['STATE'];
    }
    public static function get_icon($id,$ajax=false){
        return dbHandler::dql('SELECT REPO_ICON FROM repo WHERE REPO_ID = :id', array(':id'=>$id),$ajax)['REPO_ICON'];
    }
    public static function set_icon($id,$ext,$ajax=false){
        $param = array(':id'=>$id,':ext'=>$ext,':client'=>$_SESSION['id']);
        return dbHandler::dql('SELECT repo_icon(:ext,:id,:client) STATE', $param,$ajax)['STATE'];
    }
}
abstract class Figure{
    public static function ls($id){
        $param = array(':id'=>$id);
        return dbHandler::dql('SELECT f.*,USERNAME FROM figure f, repo r, user u WHERE f.REPO_ID = r.REPO_ID AND r.USER_ID = u.USER_ID AND f.REPO_ID = :id', $param, TRUE);
    }

    public static function add($label,$desc,$type,$repo,$ajax=false){
        $param = array(':label'=>$label,':desc'=>$desc,':type'=>$type,':repo'=>$repo,':client'=>$_SESSION['id']);
        return dbHandler::dql('SELECT add_figure(:label,:desc,:type,:repo,:client) ID', $param, $ajax)['ID'];
    }
    public static function ls_types($ajax=false){
        return dbHandler::dql('SELECT DISTINCT FIGURE_TYPE FROM figure WHERE FIGURE_TYPE <> \'\'', null, $ajax);
    }

    public static function shortlist($rid,$ftype){
        $param = array(':rid'=>$rid,':ftype'=>$ftype);
        return dbHandler::dql('SELECT * FROM figure WHERE REPO_ID = :rid AND FIGURE_TYPE = :ftype',$param,TRUE);
    }

    public static function update($label,$desc,$type,$id,$ajax=false){
        $param = array(':label'=>$label,':desc'=>$desc,':type'=>$type,':fid'=>$id,':client'=>$_SESSION['id']);
        return dbHandler::dql('SELECT mv_figure(:label,:desc,:type,:fid,:client) STATE', $param, $ajax)['STATE'];
    }

    public static function rm($id,$ajax = false){
        $param = array(':fid'=>$id,':client'=>$_SESSION['id']);
        return dbHandler::dql('SELECT rm_figure(:fid,:client) STATE', $param, $ajax)['STATE'];
    }
}
abstract class Quest {
    public static function focus($qid,$action){
        $param = array(':qid'=>$qid,':action'=>$action,':uid'=>$_SESSION['id']);
        return dbHandler::execute('INSERT INTO focus_quest(QUEST_ID,Q_ACTION,USER_ID) VALUES(:qid,:action,:uid)', $param, TRUE);
    }

    public static function add($repo, $qtype,$ajax=false) {
        $param = array(':rid'=>$repo,':qtype'=>$qtype,':client'=>$_SESSION['id']);
        return dbHandler::dql('SELECT add_quest(:rid,:qtype,:client) ID',$param,$ajax)['ID'];
    }
    public static function rm($id){
        return dbHandler::dql('SELECT rm_quest(:id,:client) STATE', array(':id'=>$id,':client'=>$_SESSION['id']), true)['STATE'];
    }
    public static function ls($repo,$ajax=false){
        $param = array(':rid'=>$repo);
        return dbHandler::dql('SELECT * FROM QUEST_LIST WHERE REPO = :rid', $param, $ajax);
    }
    public static function ls_ready($repo,$ajax=false){
        $param = array(':rid'=>$repo);
        return dbHandler::dql('SELECT * FROM QUEST_LIST WHERE READY = TRUE AND REPO = :rid', $param, $ajax);
    }
    public static function ls_text($id){
        return dbHandler::dql(' SELECT qt.*,l.LANG_DESC FROM quest_text qt, language l WHERE qt.LANG_ID = l.LANG_ID AND QUEST_ID = :id', array(':id'=>$id), true);
    }
    public static function ls_ansr($id){
        $param = array(':id'=>$id);
        return dbHandler::dql('SELECT a.*,QTYPE_ID FROM answer a,question q WHERE q.QUEST_ID = a.QUEST_ID AND a.QUEST_ID = :id', $param, true);
    }
    public static function ls_fig($id){
        return dbHandler::dql('SELECT f.*,qf.QUEST_ID FROM quest_figure qf, figure f WHERE qf.FIGURE_ID = f.FIGURE_ID AND QUEST_ID = :id', array(':id'=>$id), true);
    }

    public static function get_state($id){
        return dbHandler::dql('SELECT READY FROM question WHERE QUEST_ID = :id',array(':id'=>$id),TRUE)['READY'];
    }
    public static function get_type($id){
        return dbHandler::dql('SELECT QTYPE_ID FROM question WHERE QUEST_ID = :id',array(':id'=>$id),TRUE)['QTYPE_ID'];
    }

    public static function add_text($qid,$lid,$text){
        $param= array(':qid'=>$qid,':lid'=>$lid,':text'=>$text,':client'=>$_SESSION['id']);
        return dbHandler::dql('SELECT add_quest_text(:qid,:lid,:text,:client) STATUS',$param,true)['STATUS'];
    }
    public static function rm_text($qid,$lid){
        $param = array(':qid'=>$qid,':lid'=>$lid,':subject'=>'text',':client'=>$_SESSION['id']);
        return dbHandler::dql('SELECT rm_quest_cmp(:qid,:subject,:lid,:client) REPORT', $param, true)['REPORT'];
    }
    public static function add_answer($qid,$txt,$sound,$misnomer){
        $param = array(':qid'=>$qid,':txt'=>$txt,':sound'=>$sound,':misnomer'=>$misnomer,':client'=>$_SESSION['id']);
        return dbHandler::dql('SELECT add_quest_ansr(:qid,:txt,:sound,:misnomer,:client) REPORT', $param, TRUE)['REPORT'];
    }
    public static function rm_answer($id){
        $param = array(':id'=>$id,':client'=>$_SESSION['id']);
        return dbHandler::dql('SELECT rm_quest_cmp(:id,\'ansr\',null,:client) REPORT', $param, true)['REPORT'];
    }
    public static function get_answer_id($qid,$ansr){
        $param = array(':qid'=>$qid,':ansr'=>$ansr);
        return dbHandler::dql('SELECT ANS_ID FROM answer WHERE QUEST_ID = :qid AND TEXT = :ansr', $param, true)['ANS_ID'];
    }
    public static function get_quest_id_by_ansr($aid){
        return dbHandler::dql('SELECT QUEST_ID FROM answer WHERE ANS_ID = :aid', array(':aid'=>$aid), true)['QUEST_ID'];
    }

    public static function get_figures($id){
        return dbHandler::dql('SELECT f.* FROM figure f, quest_figure qf where f.FIGURE_ID = qf.FIGURE_ID AND qf.QUEST_ID = :id', array(':id'=>$id), true);
    }
    public static function set_figure($qid,$fid,$action){
        $param = array(':qid'=>$qid,':fid'=>$fid,':action'=>$action,':client'=>$_SESSION['id']);
        return dbHandler::dql('SELECT assign_quest_figure(:qid,:fid,:action,:client) REPLY', $param,true)['REPLY'];
    }
}
abstract class Lang{
    public static function not_in_quest($qid){
        $param = array(':qid'=>$qid);
        return dbHandler::dql('SELECT * FROM language WHERE LANG_ID NOT IN (SELECT LANG_ID FROM quest_text WHERE QUEST_ID = :qid)', $param, true);
    }
}

abstract class Item2{
    public static function ls(){
        $param = array(':uid'=>$_SESSION['id']);
        return dbHandler::dql('SELECT * FROM topic t, topic_type tt WHERE t.TTYPE_ID = tt.TTYPE_ID AND USER_ID = :uid', $param, true);
    }
    public static function get_this($id){
        $param = array(':id'=>$id);
        return dbHandler::dql('SELECT * FROM topic t, topic_type tt WHERE t.TTYPE_ID = tt.TTYPE_ID AND t.TOPIC_ID = :id', $param, TRUE);
    }

    public static function ls_quest($id){
        $param = array(':id'=>$id);
        return dbHandler::dql('SELECT q.* FROM QUEST_LIST q, quest_topic qt WHERE q.ID = qt.QUEST_ID AND qt.TOPIC_ID = :id', $param, true);
    }
    
    public static function ls_repo_quest($id,$repo){
        $param = array(':id'=>$id,':rid'=>$repo);
        $sql = 'SELECT * FROM QUEST_LIST WHERE READY = TRUE AND REPO = :rid AND ID NOT IN (SELECT QUEST_ID FROM quest_topic WHERE TOPIC_ID = :id)';
        return dbHandler::dql($sql, $param, TRUE);
    }

    public static function add($ttype,$ttitle){
        $param = array(':ttype'=>$ttype,':ttitle'=>$ttitle,':uid'=>$_SESSION['id']);
        return dbHandler::execute('INSERT INTO topic(TOPIC_TITLE,TTYPE_ID,USER_ID) VALUES(:ttitle,:ttype,:uid)', $param, TRUE);
    }
    public static function update($ttype,$ttitle,$id){
        $param = array(':ttype'=>$ttype,':ttitle'=>$ttitle,':id'=>$id);
        return dbHandler::execute('UPDATE topic SET TOPIC_TITLE = :ttitle, TTYPE_ID = :ttype WHERE TOPIC_ID = :id', $param, true);
    }
    public static function add_quest($id,$qid){
        $param = array(':id'=>$id,':qid'=>$qid);
        return dbHandler::execute('INSERT INTO quest_topic VALUES(:qid,:id)', $param, TRUE);
    }
    public static function rm_quest($id,$qid){
        $param = array(':id'=>$id,':qid'=>$qid);
        return dbHandler::execute('DELETE FROM quest_topic WHERE QUEST_ID = :qid AND TOPIC_ID = :id', $param, TRUE);
    }
}

abstract class Item4{
    public static function repo_quest(){
        return dbHandler::dql("SELECT REPO_NAME, COUNT(*) FROM repo r, question q WHERE q.REPO_ID = r.REPO_ID AND USER_ID = :uid GROUP BY REPO_NAME;", array(':uid'), true);
    }
    public static function focus_hits($qid,$action,$date){
        $param = array(':action'=>$action,':date'=>$date,':qid'=>$qid);
        return dbHandler::dql('SELECT sf_focus_hits(:qid,:action,:date) HITS', $param, true)['HITS'];
    }
}